
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Typeconsult;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface TypeconsultService extends Service<Typeconsult, String>{
    
}