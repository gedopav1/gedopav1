
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Specialite;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface SpecialiteService extends Service<Specialite, Integer>{
    
}