
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.entities.PrescriptionPK;
import com.mte.gedopa.service.Service;

/**
 *
 * @author abalo
 */

public interface PrescriptionService extends Service<Prescription, PrescriptionPK>{
    
}