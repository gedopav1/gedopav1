
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.service.Service;

/**
 *
 * @author H
 */
public interface PatientService extends Service<Patient, Long>{

    /**
     * Recherche une annonce escale de la BD à partir de son num_AAN
     * @param code le code ATP envoyé par le GUCE
     * @return l'annonce escale dont le num_AAN correspond au code fourni ou null si aucune correspondance trouvée
     * @throws java.lang.Exception
     */
    public Patient findByCode(String code) throws Exception;
}