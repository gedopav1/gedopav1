
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Hypothesediag;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface HypothesediagService extends Service<Hypothesediag, Integer>{
    
}