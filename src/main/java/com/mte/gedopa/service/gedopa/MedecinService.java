
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface MedecinService extends Service<Medecin, String>{
    
}