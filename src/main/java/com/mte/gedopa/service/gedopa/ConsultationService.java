
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface ConsultationService extends Service<Consultation, Long>{
    
}