
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.SysParam;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface SysParamService extends Service<SysParam, String>{
    
    public String getValue(String sysParamCode) throws Exception;
    
}