
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Antecedent;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface AntecedentService extends Service<Antecedent, Integer>{
    
}