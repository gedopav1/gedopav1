
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Medicament;
import com.mte.gedopa.service.Service;


/**
 *
 * @author abalo
 */
public interface MedicamentService extends Service<Medicament, String>{
    
}