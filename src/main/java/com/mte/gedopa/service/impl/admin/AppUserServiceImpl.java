
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.Menu;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.entities.ProfilMenu;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.entities.UserMenu;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.AppUserService;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;
import org.siconsult.cipher.mediacipher.DataHasher;


/**
 *
 * @author H
 */
public class AppUserServiceImpl extends ServiceImpl<AppUser, String> implements AppUserService{
    
    private static AppUserServiceImpl instance;
    
    private AppUserServiceImpl(){
        
    }
    
    public static AppUserServiceImpl getInstance(){
        if(instance == null){
            instance = new AppUserServiceImpl();
        }
        return instance;
    }

    @Override
    public AppUser authentifier(String login, String password, String operDesc) throws Exception{
        AppUser appUser = null;
       // DataHasher hasher = new DataHasher();
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("userLogin", OperateurCond.EQUAL, login));
     // criteres.add(new Critere("userPassword", OperateurCond.EQUAL, hasher.generateSHA(password)));
        criteres.add(new Critere("userPassword", OperateurCond.EQUAL, password));
        criteres.add(new Critere("actif", OperateurCond.EQUAL, Boolean.TRUE));
        System.out.println("login ----- " + login + "pass --- " + password);
        List<AppUser> list = findBy(criteres);
        System.out.println("list ----- " + list);
        System.out.println("list ----- " + list.get(0)); 
        appUser = list.get(0);
        if(list != null && list.size() == 1){
            appUser = list.get(0);
            log("AppUser", appUser, ActionServiceImpl.getInstance().findByPk("LOGIN"), operDesc, true);
        }
        
        return appUser;
    }
    
    @Override
    public void logout(AppUser appUser, String operDesc) throws Exception{
        log("AppUser", appUser, ActionServiceImpl.getInstance().findByPk("LOGOUT"), operDesc, true);
    }
    
    
    @Override
    public List<Menu> findMenusByProfil(Profil profil, boolean actifOnly){
        List<Menu> list = new ArrayList<>();
        if(profil == null){
            return list;
        }
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("profil.profilCode", OperateurCond.EQUAL, profil.getProfilCode()));
        if(actifOnly){
            criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        }
        try {
            List<ProfilMenu> profilMenus = ProfilMenuServiceImpl.getInstance().findBy(criteres);
            if(profilMenus != null && !profilMenus.isEmpty()){
                for (ProfilMenu profilMenu : profilMenus) {
                    list.add(profilMenu.getMenu());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AppUserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    
    @Override
    public List<Menu> findMenusByUser(AppUser appUser, boolean actifOnly){
        List<Menu> list = new ArrayList<>();
        if(appUser == null){
            return list;
        }
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("appUser.userLogin", OperateurCond.EQUAL, appUser.getUserLogin()));
        if(actifOnly){
            criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        }
        try {
            List<UserMenu> userMenus = UserMenuServiceImpl.getInstance().findBy(criteres);
            if(userMenus != null && !userMenus.isEmpty()){
                for (UserMenu userMenu : userMenus) {
                    list.add(userMenu.getMenu());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AppUserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    
    @Override
    public List<Menu> findMenus(AppUser appUser, boolean actifOnly){
        List<Menu> list = new ArrayList<>();
        list.addAll(findMenusByProfil(appUser != null ? appUser.getProfilCode() : null, actifOnly));
        list.addAll(findMenusByUser(appUser, actifOnly));
        return list;
    }
    
    @Override
    public List<Report> findReports(AppUser appUser, boolean actifOnly){
        List<Report> list = new ArrayList<>();
        list.addAll(ProfilReportServiceImpl.getInstance().findByProfil(appUser != null ? appUser.getProfilCode() : null, actifOnly));
        list.addAll(UserReportServiceImpl.getInstance().findByUser(appUser, actifOnly));
        return list;
    }
}