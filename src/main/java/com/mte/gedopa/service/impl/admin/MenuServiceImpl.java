
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Menu;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.MenuService;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import com.mte.gedopa.util.OperateurSql;
import com.mte.gedopa.util.SortConfig;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author H
 */
public class MenuServiceImpl extends ServiceImpl<Menu, Integer> implements MenuService{
    
    private static MenuServiceImpl instance;
    
    private MenuServiceImpl(){
        
    }
    
    public static MenuServiceImpl getInstance(){
        if(instance == null){
            instance = new MenuServiceImpl();
        }
        return instance;
    }

    @Override
    public List<Menu> getTopMenus() throws Exception {
        List list = new ArrayList();
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        criteres.add(new Critere("menuParent", OperateurCond.EQUAL, "null"));//ceux dont le menu_parent est null sont les TOP
            List<SortConfig> sortConfigs = new ArrayList<>();
            sortConfigs.add(new SortConfig("numOrdre"));
        list.addAll(findBy(criteres, sortConfigs));
        return list;
    }

    @Override
    public List<Menu> getChildMenus(Menu parent) throws Exception {
        List list = new ArrayList();
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        criteres.add(new Critere("menuParent", OperateurCond.EQUAL, parent));
            List<SortConfig> sortConfigs = new ArrayList<>();
            sortConfigs.add(new SortConfig("numOrdre"));
        list.addAll(findBy(criteres, sortConfigs));
        return list;
    }

    @Override
    public void addChildrenToWorkingList(List<Menu> workingList, Menu menu, boolean addChildren) throws Exception {
        //on l'ajoute au workingList s'il n'y est pas déjà
        if (!workingList.contains(menu)) {
            workingList.add(menu);
        }
        //on ajoute son menu parent au workingList s'il n'y est pas déjà
        if (menu != null && menu.getMenuParent() != null && !workingList.contains(menu.getMenuParent())) {
            addChildrenToWorkingList(workingList, menu.getMenuParent(), false);//false pour ajouter uniqmt le menu parent mais pas les enfants du parent
        }
        if (addChildren) {
            List<Menu> children = getChildMenus(menu);
            //on ajoute tous ses enfants et les enfants de leurs enfants au workingList s'ils n'y sont pas déjà
            for (Menu child : children) {
                addChildrenToWorkingList(workingList, child, true);//true pour ajouter le menu  et tous ses enfants
            }
        }
    }

    @Override
    public List<Menu> getActifMenus() throws Exception {
        List list = new ArrayList();
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        list.addAll(findBy(criteres));
        return list;
    }
    
}