
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.UserApp;
import com.mte.gedopa.entities.UserAppPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.UserAppService;


/**
 *
 * @author H
 */
public class UserAppServiceImpl extends ServiceImpl<UserApp, UserAppPK> implements UserAppService{
    
    private static UserAppServiceImpl instance;
    
    private UserAppServiceImpl(){
        
    }
    
    public static UserAppServiceImpl getInstance(){
        if(instance == null){
            instance = new UserAppServiceImpl();
        }
        return instance;
    }
}