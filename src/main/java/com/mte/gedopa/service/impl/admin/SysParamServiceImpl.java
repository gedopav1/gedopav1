package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.SysParam;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.SysParamService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author H
 */
public class SysParamServiceImpl extends ServiceImpl<SysParam, String> implements SysParamService {

    private static SysParamServiceImpl instance;

    private SysParamServiceImpl() {

    }

    public static SysParamServiceImpl getInstance() {
        if (instance == null) {
            instance = new SysParamServiceImpl();
        }
        return instance;
    }

    @Override
    public String getValue(String sysParamCode) throws Exception {
        String value = null;

        SysParam sp;
        try {
            sp = findByPk(sysParamCode);
            if (sp != null) {
                value = sp.getSysParamValue();
            }
            else{
                throw new Exception("Le paramètre " + sysParamCode + " n'existe pas!");
            }
        } catch (Exception ex) {
            throw ex;
        }

        return value;
    }
}
