
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Typeconsult;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.TypeconsultService;


/**
 *
 * @author H
 */
public class TypeconsultServiceImpl extends ServiceImpl<Typeconsult, String> implements TypeconsultService{
    
    private static TypeconsultServiceImpl instance;
    
    private TypeconsultServiceImpl(){
        
    }
    
    public static TypeconsultServiceImpl getInstance(){
        if(instance == null){
            instance = new TypeconsultServiceImpl();
        }
        return instance;
    }
}