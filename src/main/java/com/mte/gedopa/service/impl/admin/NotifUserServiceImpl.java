
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.NotifUser;
import com.mte.gedopa.entities.NotifUserPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.NotifUserService;


/**
 *
 * @author H
 */
public class NotifUserServiceImpl extends ServiceImpl<NotifUser, NotifUserPK> implements NotifUserService{
    
    private static NotifUserServiceImpl instance;
    
    private NotifUserServiceImpl(){
        
    }
    
    public static NotifUserServiceImpl getInstance(){
        if(instance == null){
            instance = new NotifUserServiceImpl();
        }
        return instance;
    }
}