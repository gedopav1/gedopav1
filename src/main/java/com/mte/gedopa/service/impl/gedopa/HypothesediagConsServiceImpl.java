
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.HypothesediagCons;
import com.mte.gedopa.entities.HypothesediagConsPK;
import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.entities.PrescriptionPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.HypothesediagConsService;
import com.mte.gedopa.service.gedopa.HypothesediagConsService;


/**
 *
 * @author H
 */
public class HypothesediagConsServiceImpl extends ServiceImpl<HypothesediagCons, HypothesediagConsPK> implements HypothesediagConsService{
    
    private static HypothesediagConsServiceImpl instance;
    
    private HypothesediagConsServiceImpl(){
        
    }
    
    public static HypothesediagConsServiceImpl getInstance(){
        if(instance == null){
            instance = new HypothesediagConsServiceImpl();
        }
        return instance;
    }
}