
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.ProfilMenu;
import com.mte.gedopa.entities.ProfilMenuPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ProfilMenuService;


/**
 *
 * @author H
 */
public class ProfilMenuServiceImpl extends ServiceImpl<ProfilMenu, ProfilMenuPK> implements ProfilMenuService{
    
    private static ProfilMenuServiceImpl instance;
    
    private ProfilMenuServiceImpl(){
        
    }
    
    public static ProfilMenuServiceImpl getInstance(){
        if(instance == null){
            instance = new ProfilMenuServiceImpl();
        }
        return instance;
    }
}