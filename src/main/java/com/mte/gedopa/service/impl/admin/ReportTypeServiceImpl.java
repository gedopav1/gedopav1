
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.ReportType;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ReportTypeService;


/**
 *
 * @author H
 */
public class ReportTypeServiceImpl extends ServiceImpl<ReportType, String> implements ReportTypeService{
    
    private static ReportTypeServiceImpl instance;
    
    private ReportTypeServiceImpl(){
        
    }
    
    public static ReportTypeServiceImpl getInstance(){
        if(instance == null){
            instance = new ReportTypeServiceImpl();
        }
        return instance;
    }
}