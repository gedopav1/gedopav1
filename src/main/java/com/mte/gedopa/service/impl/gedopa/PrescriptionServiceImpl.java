
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.entities.PrescriptionPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.PrescriptionService;


/**
 *
 * @author H
 */
public class PrescriptionServiceImpl extends ServiceImpl<Prescription, PrescriptionPK> implements PrescriptionService{
    
    private static PrescriptionServiceImpl instance;
    
    private PrescriptionServiceImpl(){
        
    }
    
    public static PrescriptionServiceImpl getInstance(){
        if(instance == null){
            instance = new PrescriptionServiceImpl();
        }
        return instance;
    }
}