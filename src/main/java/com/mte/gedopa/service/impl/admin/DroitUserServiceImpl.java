
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.DroitUser;
import com.mte.gedopa.entities.DroitUserPK;
import com.mte.gedopa.entities.Screen;
import com.mte.gedopa.entities.UserMenu;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.DroitUserService;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author H
 */
public class DroitUserServiceImpl extends ServiceImpl<DroitUser, DroitUserPK> implements DroitUserService{
    
    private static DroitUserServiceImpl instance;
    
    private DroitUserServiceImpl(){
        
    }
    
    public static DroitUserServiceImpl getInstance(){
        if(instance == null){
            instance = new DroitUserServiceImpl();
        }
        return instance;
    }
    

    @Override
    public String listActionsByUserMenu(UserMenu userMenu) throws Exception {
        String listAsString = "";
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("appUser.userLogin", OperateurCond.EQUAL, userMenu.getAppUser().getUserLogin()));
        criteres.add(new Critere("screen.screenId", OperateurCond.EQUAL, userMenu.getMenu().getScreenId().getScreenId()));
        List<DroitUser> droits = findBy(criteres);

        if (droits != null && droits.size() >= 1) {
            listAsString += droits.get(0).getAction().getActionNom();
            for (DroitUser droitUser : droits.subList(1, droits.size())) {
                listAsString += " / " + droitUser.getAction().getActionNom();
            }
        }

        return listAsString;
    }

    @Override
    public List<DroitUser> listByUserAndScreen(AppUser appUser, Screen screen) throws Exception {
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("appUser.userLogin", OperateurCond.EQUAL, appUser.getUserLogin()));
        criteres.add(new Critere("screen.screenId", OperateurCond.EQUAL, screen.getScreenId()));
        return findBy(criteres);
    }
}