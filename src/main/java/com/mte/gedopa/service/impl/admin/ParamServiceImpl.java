
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Param;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ParamService;


/**
 *
 * @author H
 */
public class ParamServiceImpl extends ServiceImpl<Param, Integer> implements ParamService{
    
    private static ParamServiceImpl instance;
    
    private ParamServiceImpl(){
        
    }
    
    public static ParamServiceImpl getInstance(){
        if(instance == null){
            instance = new ParamServiceImpl();
        }
        return instance;
    }
}