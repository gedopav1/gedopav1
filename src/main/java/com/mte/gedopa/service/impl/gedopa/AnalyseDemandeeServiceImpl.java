
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.AnalyseDemandee;
import com.mte.gedopa.entities.AnalyseDemandeePK;
import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.entities.PrescriptionPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.AnalyseDemandeeService;
import com.mte.gedopa.service.gedopa.PrescriptionService;


/**
 *
 * @author H
 */
public class AnalyseDemandeeServiceImpl extends ServiceImpl<AnalyseDemandee, AnalyseDemandeePK> implements AnalyseDemandeeService{
    
    private static AnalyseDemandeeServiceImpl instance;
    
    private AnalyseDemandeeServiceImpl(){
        
    }
    
    public static AnalyseDemandeeServiceImpl getInstance(){
        if(instance == null){
            instance = new AnalyseDemandeeServiceImpl();
        }
        return instance;
    }
}