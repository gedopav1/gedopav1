
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Menu;
import com.mte.gedopa.service.Service;
import java.util.List;


/**
 *
 * @author H
 */
public interface MenuService extends Service<Menu, Integer>{
    
    /**
     * Liste tous les menus de 1er niveau; c-à-d ceux qui n'ont pas de parent
     * @return la liste des menus de 1er niveau
     * @throws java.lang.Exception
     */
    public List<Menu> getTopMenus() throws Exception;
    
    /**
     * Liste tous les menus fils d'un menu donné en paramètre
     * @param parent Le menu parent dont on liste les fils
     * @return la liste des menus fils ou une liste vide
     * @throws java.lang.Exception
     */
    public List<Menu> getChildMenus(Menu parent) throws Exception;
    
    /**
     * Ajoute le menu passé en paramètre dans la liste workingList ainsi que ous ses enfants et les enfants de leurs enfants de façon reccursive
     * @param workingList
     * @param menu
     * @param addChildren
     * @throws Exception
     */
    public void addChildrenToWorkingList(List<Menu> workingList, Menu menu, boolean addChildren) throws Exception;
    
    /**
     * Liste tous les menus Actifs uniquement
     * @return la liste des menus fils ou une liste vide
     * @throws java.lang.Exception
     */
    public List<Menu> getActifMenus() throws Exception;
}