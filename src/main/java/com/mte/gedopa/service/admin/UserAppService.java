
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.UserApp;
import com.mte.gedopa.entities.UserAppPK;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface UserAppService extends Service<UserApp, UserAppPK>{
    
}