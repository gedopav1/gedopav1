
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.NotifUser;
import com.mte.gedopa.entities.NotifUserPK;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface NotifUserService extends Service<NotifUser, NotifUserPK>{
    
}