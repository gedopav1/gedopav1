
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.SysLog;
import com.mte.gedopa.service.Service;
import java.math.BigDecimal;


/**
 *
 * @author H
 */
public interface SysLogService extends Service<SysLog, BigDecimal>{
    
}