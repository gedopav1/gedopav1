
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ReportService extends Service<Report, Integer>{
    
}