/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.dao;

import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.CritereGroup;
import com.mte.gedopa.util.CritereSimple;
import com.mte.gedopa.util.ICritere;
import com.mte.gedopa.util.MinMax;
import com.mte.gedopa.util.OperateurCond;
import com.mte.gedopa.util.OperateurSql;
import com.mte.gedopa.util.SortConfig;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 *
 * @author (SIC)
 * @param <T>
 * @param <K>
 */
public class DaoImpl<T, K> implements Dao<T, K> {

    protected EntityManager em;
    protected PersistenceService ps;

    public DaoImpl() {
        ps = PersistenceService.getInstance();
        em = ps.getEntityManager();
    }

    @Override
    public List<T> read() throws Exception {
        return read(null);
    }

    private String generateSortClause(List<SortConfig> sortConfigs, String tableAlias) {
        String sortClause = "";
        if (sortConfigs != null && !sortConfigs.isEmpty() && tableAlias != null) {
            sortClause = " order by";
            for (int i = 0; i < sortConfigs.size(); i++) {
                SortConfig sortConfig = sortConfigs.get(i);
                sortClause += " " + tableAlias + "." + sortConfig.getPropriete() + " " + sortConfig.getSortOrder().toString();
                if (i != sortConfigs.size() - 1) {//si c'est pas le dernier, on ajoute "," avant le suivant
                    sortClause += ",";
                }
            }
        }
        return sortClause;
    }

    @Override
    public List<T> read(List<SortConfig> sortConfigs) throws Exception {
        String query = "SELECT t FROM " + getEntityClass().getName() + " t";
        query += generateSortClause(sortConfigs, "t");
        Query q = getEm().createQuery(query);
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        return q.getResultList();
    }

    @Override
    public T create(T entity) throws Exception {
        return create(entity, true);
    }

    @Override
    public T update(T entity) throws Exception {
        return update(entity, true);
    }

    @Override
    public T merge(T entity) throws Exception {
        return merge(entity, true);
    }

    @Override
    public void delete(T entity) throws Exception {
        delete(entity, true);
    }

    @Override
    public T findByPk(K pk) throws Exception {
        return getEm().find(getEntityClass(), pk);
    }

    @Override
    public List<T> findBy(Map<String, Object> params, OperateurSql op) throws Exception {
        String query = "SELECT t FROM " + getEntityClass().getName() + " t ";
        int compteur = 0;
        String type;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            /* Vérification d'opérateur logique : AND,OR,WHERE */
            query += ((compteur++ != 0) ? op.toString() : "WHERE");

            /* Conversion proprement dite */
            String condition = null;
            if (entry.getValue() == null || "null".equalsIgnoreCase(entry.getValue().toString())) {
                condition = " is null ";
            } else if ("!null".equalsIgnoreCase(entry.getValue().toString())) {
                condition = " is not null ";
            } else {
                type = (entry.getValue().getClass().getSimpleName().equals("Date") || entry.getValue().getClass().getSimpleName().equalsIgnoreCase("Timestamp")) ? "DATE" : "VARCHAR";
                condition = ("DATE".equals(type) ? (entry.getValue().toString().startsWith("!") ? " <> " : " = ") : (entry.getValue().toString().startsWith("!") ? " NOT LIKE " : " LIKE ")) + ":p" + compteur + " ";
            }
            query += " t." + entry.getKey() + condition;

        }
        System.out.println(query);
        compteur = 0;
        Query q = getEm().createQuery(query);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            ++compteur;
            if (!(entry.getValue() == null || "null".equalsIgnoreCase(entry.getValue().toString()) || "!null".equalsIgnoreCase(entry.getValue().toString()))) {
                q.setParameter("p" + (compteur), entry.getValue());
            }

        }
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        return q.getResultList();

    }

    @Override
    public Class<T> getEntityClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * @return the em
     */
    public EntityManager getEm() {
        return em;
    }

    /**
     * @return the ps
     */
    public PersistenceService getPs() {
        return ps;
    }

    @Override
    public T create(T entity, boolean autocommit) throws Exception {
        try {
            if (autocommit) {
                try {
                    getPs().beginTx();
                } catch (Exception e) {
                    //do nothing
                }
            }
            getEm().persist(entity);
            if (autocommit) {
                getPs().commitTx();
            }
        } catch (Exception e) {
            if (autocommit && getPs().getEntityManager().getTransaction().isActive()) {
                getPs().rollbackTx();
            }
            throw e;
        }
        return entity;
    }

    @Override
    public T update(T entity, boolean autocommit) throws Exception {
        try {
            if (autocommit) {
                try {
                    getPs().beginTx();
                } catch (Exception e) {
                    //do nothing
                }
            }
            boolean tableAssociation = false;
            String jpql = "UPDATE " + getEntityClass().getName() + " t SET ";

            Field[] fields = entity.getClass().getDeclaredFields();
            for (Field field : fields) {
                if(field.isAnnotationPresent(EmbeddedId.class)){
                    tableAssociation = true;
                    break;
                }
            }
            int compteur = 0;
            Field idField = null;
            for (Field field : fields) {
                if (field.isAnnotationPresent(Id.class) || field.isAnnotationPresent(EmbeddedId.class)) {
                    idField = field;
                } else if (field.isAnnotationPresent(Column.class) || (field.isAnnotationPresent(ManyToOne.class) && !tableAssociation) || field.isAnnotationPresent(OneToOne.class)) {
                    jpql += (compteur++ == 0 ? "" : ", ") + " t." + field.getName() + " = :" + field.getName();
                }
            }
            if (idField == null) {
                throw new Exception("Champ de clé primaire non détecté");
            }
            jpql += " WHERE t." + idField.getName() + " = :" + idField.getName();
            Query q = getEm().createQuery(jpql);
            for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class) || (field.isAnnotationPresent(ManyToOne.class) && !tableAssociation) || field.isAnnotationPresent(Id.class) || field.isAnnotationPresent(EmbeddedId.class) || field.isAnnotationPresent(OneToOne.class)) {
                    field.setAccessible(true);
                    q.setParameter(field.getName(), field.get(entity));
                    field.setAccessible(false);
                }
            }
            int count = 0;
            try {
                count = q.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (count != 1) {
                throw new Exception("Nombre de lignes mises à jour différent de 1");
            }

            if (autocommit) {
                try {
                    getPs().commitTx();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            if (autocommit && getPs().getEntityManager().getTransaction().isActive()) {
                getPs().rollbackTx();
            }
            throw e;
        }
        return entity;
    }
    
    

    @Override
    public T merge(T entity, boolean autocommit) throws Exception {
        try {
            if (autocommit) {
                try {
                    getPs().beginTx();
                } catch (Exception e) {
                    //do nothing
                }
            }
            entity = getEm().merge(entity);
            if (autocommit) {
                getPs().commitTx();
            }
        } catch (Exception e) {
            if (autocommit && getPs().getEntityManager().getTransaction().isActive()) {
                getPs().rollbackTx();
            }
            throw e;
        }
        return entity;
    }

    @Override
    public void delete(T entity, boolean autocommit) throws Exception {
        try {
            if (autocommit) {
                try {
                    getPs().beginTx();
                } catch (Exception e) {
                    //do nothing
                }
            }
            getEm().remove(getEm().merge(entity));
            if (autocommit) {
                getPs().commitTx();
                //flushAndClearContext();
            }
        } catch (Exception e) {
            //Logger.getLogger(DaoImpl.class.getName()).log(Level.SEVERE, null, e);
            if (autocommit && getPs().getEntityManager().getTransaction().isActive()) {
                getPs().rollbackTx();
            }
            throw e;
        }
    }

    @Override
    public T clone(T original) throws Exception {
        /*Instanciation dynamique*/
        T clone = (T) Class.forName(original.getClass().getName()).newInstance();
        /*Itération sur les champs du clone pour les définir avec ceux de l'original*/
        for (Field f : clone.getClass().getDeclaredFields()) {
            /*L'attribut de sérialisation ne devra jamais changer d'état*/
            if (!f.getName().equals("serialVersionUID")) {
                /* Suppression des controles d'accessibilité pour pouvoir récupérer la valeur du champ */
                f.setAccessible(true);
                f.set(clone, f.get(original));
                f.setAccessible(false);
            }
        }

        return clone;
    }

    @Override
    public List<T> findBy(List<Critere> criteres, List<SortConfig> sortConfigs) throws Exception {
        String query = "SELECT t FROM " + getEntityClass().getName() + " t ";
        int compteur = 0;
        for (Critere critere : criteres) {
            /* Vérification d'opérateur logique : AND,OR,WHERE */
            query += ((compteur++ != 0) ? critere.getOpSql().toString() : "WHERE ");

            String condition;
            if (critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString())) {
                if (critere.getOpCond().equals(OperateurCond.EQUAL)) {
                    condition = " is null ";
                } else {
                    condition = " is not null ";
                }

            } else {// valeur non nulle
                condition = " " + critere.getOpCond() + " :p" + compteur + " ";

            }
            query += " t." + critere.getPropriete() + condition;

        }
        query += generateSortClause(sortConfigs, "t");
        System.out.println(query);
        compteur = 0;
        Query q = getEm().createQuery(query);
        for (Critere critere : criteres) {
            ++compteur;
            if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                q.setParameter("p" + (compteur), critere.getValeur());
                System.out.println("p" + (compteur) + " = " + critere.getValeur());
            }

        }
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        return q.getResultList();
    }

    private String getQueryToken(CritereSimple critere, int compteur, boolean addOpSql) {
        String resultQuery = "";
        resultQuery += (addOpSql ? critere.getOpSql().toString() : "");

        String condition;
        if (critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString())) {
            if (critere.getOpCond().equals(OperateurCond.EQUAL)) {
                condition = " is null ";
            } else {
                condition = " is not null ";
            }

        } else {// valeur non nulle
            condition = " " + critere.getOpCond() + " :p" + compteur + " ";

        }
        resultQuery += " t." + critere.getPropriete() + condition;
        return resultQuery;
    }

    @Override
    public List<T> findByIcriteres(List<ICritere> criteres, List<SortConfig> sortConfigs) throws Exception {
        String query = "SELECT t FROM " + getEntityClass().getName() + " t ";
        int compteur = 0;
        if (criteres != null && criteres.size() > 0) {
            query += "WHERE ";
        }
        int groupIndex = 0;
        for (ICritere iCritere : criteres) {
            /* Vérification d'opérateur logique : AND,OR,WHERE */
            if (iCritere instanceof CritereSimple) {
                CritereSimple critere = (CritereSimple) iCritere;
                if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                    compteur++;
                }
                query += getQueryToken(critere, compteur, (groupIndex++ != 0));
            } else if (iCritere instanceof CritereGroup) {
                CritereGroup group = (CritereGroup) iCritere;
                query += compteur == 0 ? " " : " " + group.getOpSql() + " ";
                query += "(";

                groupIndex = 0;//on réinitialise à zéro au début de chaque groupe
                for (CritereSimple critere : group.getCriteres()) {
                    if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                        compteur++;
                    }
                    query += getQueryToken(critere, compteur, (groupIndex++ != 0));
                }
                query += ") ";
            }

        }
        query += generateSortClause(sortConfigs, "t");
        System.out.println(query);
        compteur = 0;
        Query q = getEm().createQuery(query);
        for (ICritere iCritere : criteres) {

            if (iCritere instanceof CritereSimple) {
                CritereSimple critere = (CritereSimple) iCritere;
                if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                    q.setParameter("p" + (++compteur), critere.getValeur());
                    System.out.println("p" + compteur + " = " + critere.getValeur());
                }
            } else if (iCritere instanceof CritereGroup) {
                for (CritereSimple critere : ((CritereGroup) iCritere).getCriteres()) {
                    if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                        q.setParameter("p" + (++compteur), critere.getValeur());
                        System.out.println("p" + compteur + " = " + critere.getValeur());
                    }
                }
            }

        }
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        return q.getResultList();
    }

    @Override
    public K findMinMaxIdBy(List<Critere> criteres, MinMax extremum, String idProperty) throws Exception {
        String query = "SELECT " + extremum + "(t." + idProperty + ") FROM " + getEntityClass().getName() + " t ";
        int compteur = 0;
        for (Critere critere : criteres) {
            /* Vérification d'opérateur logique : AND,OR,WHERE */
            query += ((compteur++ != 0) ? critere.getOpSql().toString() : "WHERE ");

            String condition;
            if (critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString())) {
                if (critere.getOpCond().equals(OperateurCond.EQUAL)) {
                    condition = " is null ";
                } else {
                    condition = " is not null ";
                }

            } else {// valeur non nulle
                condition = " " + critere.getOpCond() + " :p" + compteur + " ";

            }
            query += " t." + critere.getPropriete() + condition;

        }
        System.out.println(query);
        compteur = 0;
        Query q = getEm().createQuery(query);
        for (Critere critere : criteres) {
            ++compteur;
            if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                q.setParameter("p" + (compteur), critere.getValeur());
            }

        }

        return (K) q.getSingleResult();
    }

    @Override
    public K findMinMaxIdByIcriteres(List<ICritere> criteres, MinMax extremum, String idProperty) throws Exception {
        String query = "SELECT " + extremum + "(t." + idProperty + ") FROM " + getEntityClass().getName() + " t ";
        int compteur = 0;
        if (criteres != null && criteres.size() > 0) {
            query += "WHERE ";
        }
        int groupIndex = 0;
        for (ICritere iCritere : criteres) {
            /* Vérification d'opérateur logique : AND,OR,WHERE */
            if (iCritere instanceof CritereSimple) {
                CritereSimple critere = (CritereSimple) iCritere;
                if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                    compteur++;
                }
                query += getQueryToken(critere, compteur, (groupIndex++ != 0));
            } else if (iCritere instanceof CritereGroup) {
                CritereGroup group = (CritereGroup) iCritere;
                query += compteur == 0 ? " " : " " + group.getOpSql() + " ";
                query += "(";

                groupIndex = 0;//on réinitialise à zéro au début de chaque groupe
                for (CritereSimple critere : group.getCriteres()) {
                    if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                        compteur++;
                    }
                    query += getQueryToken(critere, compteur, (groupIndex++ != 0));
                }
                query += ") ";
            }

        }
        System.out.println(query);
        compteur = 0;
        Query q = getEm().createQuery(query);
        for (ICritere iCritere : criteres) {

            if (iCritere instanceof CritereSimple) {
                CritereSimple critere = (CritereSimple) iCritere;
                if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                    q.setParameter("p" + (++compteur), critere.getValeur());
                    System.out.println("p" + compteur + " = " + critere.getValeur());
                }
            } else if (iCritere instanceof CritereGroup) {
                for (CritereSimple critere : ((CritereGroup) iCritere).getCriteres()) {
                    if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                        q.setParameter("p" + (++compteur), critere.getValeur());
                        System.out.println("p" + compteur + " = " + critere.getValeur());
                    }
                }
            }

        }
        //q.setHint(QueryHints.REFRESH, HintValues.TRUE);

        return (K) q.getSingleResult();
    }

    @Override
    public List<T> findBy(List<Critere> criteres) throws Exception {
        return findBy(criteres, null);
    }

    @Override
    public List<T> findByIcriteres(List<ICritere> criteres) throws Exception {
        return findByIcriteres(criteres, null);
    }

}
