/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abalo
 */
@Embeddable
public class UserMenuPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "USER_LOGIN")
    private String userLogin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MENU_ID")
    private int menuId;

    public UserMenuPK() {
    }

    public UserMenuPK(String userLogin, int menuId) {
        this.userLogin = userLogin;
        this.menuId = menuId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userLogin != null ? userLogin.hashCode() : 0);
        hash += (int) menuId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserMenuPK)) {
            return false;
        }
        UserMenuPK other = (UserMenuPK) object;
        if ((this.userLogin == null && other.userLogin != null) || (this.userLogin != null && !this.userLogin.equals(other.userLogin))) {
            return false;
        }
        if (this.menuId != other.menuId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  getClass().getSimpleName() + " :: userLogin = " + userLogin + " :: menuId = " + menuId;
    }
    
}
