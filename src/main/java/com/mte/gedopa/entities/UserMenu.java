/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "USER_MENU", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserMenu.findAll", query = "SELECT u FROM UserMenu u")})
public class UserMenu implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserMenuPK userMenuPK;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;
    @JoinColumn(name = "USER_LOGIN", referencedColumnName = "USER_LOGIN", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AppUser appUser;

    public UserMenu() {
    }

    public UserMenu(UserMenuPK userMenuPK) {
        this.userMenuPK = userMenuPK;
    }

    public UserMenu(UserMenuPK userMenuPK, Boolean actif) {
        this.userMenuPK = userMenuPK;
        this.actif = actif;
    }

    public UserMenu(String userLogin, int menuId) {
        this.userMenuPK = new UserMenuPK(userLogin, menuId);
    }

    public UserMenuPK getUserMenuPK() {
        return userMenuPK;
    }

    public void setUserMenuPK(UserMenuPK userMenuPK) {
        this.userMenuPK = userMenuPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userMenuPK != null ? userMenuPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserMenu)) {
            return false;
        }
        UserMenu other = (UserMenu) object;
        if ((this.userMenuPK == null && other.userMenuPK != null) || (this.userMenuPK != null && !this.userMenuPK.equals(other.userMenuPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + userMenuPK;
    }
    
}
