/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "DROIT_PROFIL", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DroitProfil.findAll", query = "SELECT d FROM DroitProfil d")})
public class DroitProfil implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DroitProfilPK droitProfilPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "SCREEN_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Screen screen;
    @JoinColumn(name = "PROFIL_CODE", referencedColumnName = "PROFIL_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profil profil;
    @JoinColumn(name = "ACTION_CODE", referencedColumnName = "ACTION_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Action action;

    public DroitProfil() {
    }

    public DroitProfil(DroitProfilPK droitProfilPK) {
        this.droitProfilPK = droitProfilPK;
    }

    public DroitProfil(DroitProfilPK droitProfilPK, Boolean actif) {
        this.droitProfilPK = droitProfilPK;
        this.actif = actif;
    }

    public DroitProfil(String profilCode, String actionCode, int screenId) {
        this.droitProfilPK = new DroitProfilPK(profilCode, actionCode, screenId);
    }

    public DroitProfilPK getDroitProfilPK() {
        return droitProfilPK;
    }

    public void setDroitProfilPK(DroitProfilPK droitProfilPK) {
        this.droitProfilPK = droitProfilPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (droitProfilPK != null ? droitProfilPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DroitProfil)) {
            return false;
        }
        DroitProfil other = (DroitProfil) object;
        if ((this.droitProfilPK == null && other.droitProfilPK != null) || (this.droitProfilPK != null && !this.droitProfilPK.equals(other.droitProfilPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + droitProfilPK;
    }
    
}
