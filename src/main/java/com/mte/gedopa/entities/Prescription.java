/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "prescription")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prescription.findAll", query = "SELECT f FROM Prescription f")})
public class Prescription implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private PrescriptionPK prescriptionPK;
    @Column(name = "nbrfoisprise")
    private Integer nbrfoisprise;
    @JoinColumn(name = "codemedi", referencedColumnName = "codemedi", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Medicament medicament;
    @JoinColumn(name = "idconsultaion", referencedColumnName = "idconsultaion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Consultation consultation;

    public Prescription() {
    }

    public Prescription(PrescriptionPK prescriptionPK) {
        this.prescriptionPK = prescriptionPK;
    }

    public Prescription(String codemedi, long idconsultaion) {
        this.prescriptionPK = new PrescriptionPK(codemedi, idconsultaion);
    }
     
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getPrescriptionPK() != null ? getPrescriptionPK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prescription)) {
            return false;
        }
        Prescription other = (Prescription) object;
        if ((this.getPrescriptionPK() == null && other.getPrescriptionPK() != null) || (this.getPrescriptionPK() != null && !this.prescriptionPK.equals(other.prescriptionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getPrescriptionPK();
    }

    /**
     * @return the prescriptionPK
     */
    public PrescriptionPK getPrescriptionPK() {
        return prescriptionPK;
    }

    /**
     * @param prescriptionPK the prescriptionPK to set
     */
    public void setPrescriptionPK(PrescriptionPK prescriptionPK) {
        this.prescriptionPK = prescriptionPK;
    }

    /**
     * @return the nbrfoisprise
     */
    public Integer getNbrfoisprise() {
        return nbrfoisprise;
    }

    /**
     * @param nbrfoisprise the nbrfoisprise to set
     */
    public void setNbrfoisprise(Integer nbrfoisprise) {
        this.nbrfoisprise = nbrfoisprise;
    }

    /**
     * @return the medicament
     */
    public Medicament getMedicament() {
        return medicament;
    }

    /**
     * @param medicament the medicament to set
     */
    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    /**
     * @return the consultation
     */
    public Consultation getConsultation() {
        return consultation;
    }

    /**
     * @param consultation the consultation to set
     */
    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }
    
}
