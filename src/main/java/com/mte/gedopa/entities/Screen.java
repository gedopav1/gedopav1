/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "SCREEN", schema = "PUBLIC")
@SequenceGenerator(name="S_SCREEN", allocationSize = 1, schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Screen.findAll", query = "SELECT s FROM Screen s")})
public class Screen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="S_SCREEN")
    @Basic(optional = false)
    @Column(name = "SCREEN_ID")
    private Integer screenId;
    @Size(max = 60)
    @Column(name = "SCREEN_DESC")
    private String screenDesc;
    @Size(max = 100)
    @Column(name = "URL")
    private String url;
    @JoinTable(name = "ACTION_SCREEN", schema = "PUBLIC", joinColumns = {
        @JoinColumn(name = "SCREEN_ID", referencedColumnName = "SCREEN_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "ACTION_CODE", referencedColumnName = "ACTION_CODE")})
    @ManyToMany
    private List<Action> actionList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "screenId")
    private List<Menu> menuList;
    @OneToMany(mappedBy = "screenId")
    private List<Notif> notifList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "screen")
    private List<DroitUser> droitUserList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "screen")
    private List<DroitProfil> droitProfilList;

    public Screen() {
    }

    public Screen(Integer screenId) {
        this.screenId = screenId;
    }

    public Integer getScreenId() {
        return screenId;
    }

    public void setScreenId(Integer screenId) {
        this.screenId = screenId;
    }

    public String getScreenDesc() {
        return screenDesc;
    }

    public void setScreenDesc(String screenDesc) {
        this.screenDesc = screenDesc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlTransient
    public List<Action> getActionList() {
        return actionList;
    }

    public void setActionList(List<Action> actionList) {
        this.actionList = actionList;
    }

    @XmlTransient
    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    @XmlTransient
    public List<Notif> getNotifList() {
        return notifList;
    }

    public void setNotifList(List<Notif> notifList) {
        this.notifList = notifList;
    }

    @XmlTransient
    public List<DroitUser> getDroitUserList() {
        return droitUserList;
    }

    public void setDroitUserList(List<DroitUser> droitUserList) {
        this.droitUserList = droitUserList;
    }

    @XmlTransient
    public List<DroitProfil> getDroitProfilList() {
        return droitProfilList;
    }

    public void setDroitProfilList(List<DroitProfil> droitProfilList) {
        this.droitProfilList = droitProfilList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (screenId != null ? screenId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Screen)) {
            return false;
        }
        Screen other = (Screen) object;
        if ((this.screenId == null && other.screenId != null) || (this.screenId != null && !this.screenId.equals(other.screenId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + screenId;
    }
    
}
