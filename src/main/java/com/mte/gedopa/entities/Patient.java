/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author H
 */
@Entity
@Table(name = "PATIENT")
@SequenceGenerator(name="patient_id_patient_seq", allocationSize = 1)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Patient.findAll", query = "SELECT a FROM Patient a")})
public class Patient implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="patient_id_patient_seq")
    @Basic(optional = false)
    @Column(name = "id_patient")
    private Long id_patient;
    @Column(name = "numdossier")
    private String numdossier;
    @Column(name = "nom")
    private String nom;
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "deuxieme_prenom")
    private String deuxieme_prenom;
    @Basic(optional = false)
    @Column(name = "sexe")
    private Boolean sexe;
    @Basic(optional = false)
    @Column(name = "datenaiss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datenaiss;
    @Column(name = "symbole_homo")
    private String symbole_homo;
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "date_enregistrer")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_enregistrer;
    @Column(name = "lieu_naiss")
    private String lieu_naiss;
    @Column(name = "sit_fam")
    private String sit_fam;
    @Column(name = "profession")
    private String profession;
    @Column(name = "sec_sociale")
    private String sec_sociale;
    @Column(name = "caisse")
    private String caisse;
    @Column(name = "particularite")
    private String particularite;
    @Column(name = "med_traitant")
    private String med_traitant;
    @Column(name = "correspondant")
    private String correspondant;
    @Column(name = "mots_cles")
    private String mots_cles;
    @Column(name = "photo")
    private String photo;
    @Column(name = "groupesanguin")
    private String groupesanguin;
    /*
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DeclarCodeIsps> declarCodeIspsList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DeclMaritimeSante> declMaritimeSanteList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "annonceEscale")
    private List<OperationEscale> operationEscaleList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DeclListePassagers> declListePassagersList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DmdFactPassager> dmdFactPassagerList;
    @OneToMany(mappedBy = "idAnnonceEscale")
    private List<DmdPrest> dmdPrestList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<CtrlEnlevDechet> declarEnlevDechetList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAnnonceEscale")
    private List<DmdSrvNautique> dmdSrvNautiqueList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DeclMseDangereuse> declMseDangereuseList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "annonceEscale")
    private List<CoConsignataire> coConsignataireList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DmdFactNav> dmdFactNavList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<Mouvement> mouvementList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "annonceEscale")
    private List<FiliereEscale> filiereEscaleList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "escaleDest")
    private List<HTransfertManif> hTranfertManifList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "escaleOrig")
    private List<HTransfertManif> hTranfertManifList1;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<OperManutConc> operManutConctList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<ControlePhyto> controlePhytoList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<Vaq> vaqList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<Vas> vasList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DeclarDechet> declarDechetList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<EtatDifferentiel> etatDifferentielList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<IncidentEscale> incidentEscaleList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "annonceEscale")
    private List<PortEscale> portEscaleList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<Veq> veqList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DmdFactTransbo> dmdFactTransboList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "annonceEscale")
    private List<TransboEscale> transboEscaleList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<EquipeManut> equipeManutList;
    @JoinColumn(name = "ID_TYPE_ESCALE", referencedColumnName = "ID_TYPE_ESCALE")
    @ManyToOne(optional = false)
    private TypeEscale idTypeEscale;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<Sejour> sejourList;
    @JoinColumn(name = "CODE_SITU_ESCALE", referencedColumnName = "CODE_SITU_ESCALE")
    @ManyToOne(optional = false)
    private SituationEscale codeSituEscale;
    @JoinColumn(name = "POSTE_PREVU", referencedColumnName = "CODE_POSTE")
    @ManyToOne(optional = false)
    private Poste postePrevu;
    @JoinColumn(name = "PORT_PROVENANCE", referencedColumnName = "CODE_PORT")
    @ManyToOne(optional = false)
    private Port portProvenance;
    @JoinColumn(name = "PORT_DESTINATION", referencedColumnName = "CODE_PORT")
    @ManyToOne(optional = false)
    private Port portDestination;
    @JoinColumn(name = "ID_NAVIRE", referencedColumnName = "ID_NAVIRE")
    @ManyToOne(optional = false)
    private Navire idNavire;
    @JoinColumn(name = "ID_LIGNE", referencedColumnName = "ID_LIGNE")
    @ManyToOne(optional = false)
    private LigneMaritime idLigne;
    @JoinColumn(name = "ID_FENETRE_ACCOSTAGE", referencedColumnName = "ID_FENETRE_ACCOSTAGE")
    @ManyToOne
    private FenetreAccostage idFenetreAccostage;
    @JoinColumn(name = "CONSIGNATAIRE_ARRIVEE", referencedColumnName = "ID_CLIENT")
    @ManyToOne(optional = false)
    private Consignataire consignataireArrivee;
    @JoinColumn(name = "CONSIGNATAIRE_DEPART", referencedColumnName = "ID_CLIENT")
    @ManyToOne(optional = false)
    private Consignataire consignataireDepart;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<PassagerVue> passagerVueList;
    @OneToMany(mappedBy = "idAnnonceEscale")
    private List<Reserve> reserveList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<HistoAnnonceEscale> histoAnnonceEscaleList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<Manifeste> manifesteList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DeclarEquipage> declarEquipageList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<DmdFactManutBord> dmdFactManutBordList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<RapportEscale> rapportEscaleList;
    */

    public Patient() {
    }

    public Patient(Long id_patient) {
        this.id_patient = id_patient;
    }

    /*
    @XmlTransient
    public List<DeclarCodeIsps> getDeclarCodeIspsList() {
        return declarCodeIspsList;
    }

    public void setDeclarCodeIspsList(List<DeclarCodeIsps> declarCodeIspsList) {
        this.declarCodeIspsList = declarCodeIspsList;
    }
    */
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId_patient() != null ? getId_patient().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patient)) {
            return false;
        }
        Patient other = (Patient) object;
        if ((this.getId_patient() == null && other.getId_patient() != null) || (this.getId_patient() != null && !this.id_patient.equals(other.id_patient))) {
            return false;
        }
        return true;
    }
        
    @Override
    public String toString() {
       return getClass().getSimpleName() + " :: ID = " + getId_patient();
    }

    /**
     * @return the id_patient
     */
    public Long getId_patient() {
        return id_patient;
    }

    /**
     * @param id_patient the id_patient to set
     */
    public void setId_patient(Long id_patient) {
        this.id_patient = id_patient;
    }

    /**
     * @return the numdossier
     */
    public String getNumdossier() {
        return numdossier;
    }

    /**
     * @param numdossier the numdossier to set
     */
    public void setNumdossier(String numdossier) {
        this.numdossier = numdossier;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the deuxieme_prenom
     */
    public String getDeuxieme_prenom() {
        return deuxieme_prenom;
    }

    /**
     * @param deuxieme_prenom the deuxieme_prenom to set
     */
    public void setDeuxieme_prenom(String deuxieme_prenom) {
        this.deuxieme_prenom = deuxieme_prenom;
    }

    /**
     * @return the sexe
     */
    public Boolean getSexe() {
        return sexe;
    }

    /**
     * @param sexe the sexe to set
     */
    public void setSexe(Boolean sexe) {
        this.sexe = sexe;
    }

    /**
     * @return the datenaiss
     */
    public Date getDatenaiss() {
        return datenaiss;
    }

    /**
     * @param datenaiss the datenaiss to set
     */
    public void setDatenaiss(Date datenaiss) {
        this.datenaiss = datenaiss;
    }

    /**
     * @return the symbole_homo
     */
    public String getSymbole_homo() {
        return symbole_homo;
    }

    /**
     * @param symbole_homo the symbole_homo to set
     */
    public void setSymbole_homo(String symbole_homo) {
        this.symbole_homo = symbole_homo;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the date_enregistrer
     */
    public Date getDate_enregistrer() {
        return date_enregistrer;
    }

    /**
     * @param date_enregistrer the date_enregistrer to set
     */
    public void setDate_enregistrer(Date date_enregistrer) {
        this.date_enregistrer = date_enregistrer;
    }

    /**
     * @return the lieu_naiss
     */
    public String getLieu_naiss() {
        return lieu_naiss;
    }

    /**
     * @param lieu_naiss the lieu_naiss to set
     */
    public void setLieu_naiss(String lieu_naiss) {
        this.lieu_naiss = lieu_naiss;
    }

    /**
     * @return the sit_fam
     */
    public String getSit_fam() {
        return sit_fam;
    }

    /**
     * @param sit_fam the sit_fam to set
     */
    public void setSit_fam(String sit_fam) {
        this.sit_fam = sit_fam;
    }

    /**
     * @return the profession
     */
    public String getProfession() {
        return profession;
    }

    /**
     * @param profession the profession to set
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }

    /**
     * @return the sec_sociale
     */
    public String getSec_sociale() {
        return sec_sociale;
    }

    /**
     * @param sec_sociale the sec_sociale to set
     */
    public void setSec_sociale(String sec_sociale) {
        this.sec_sociale = sec_sociale;
    }

    /**
     * @return the caisse
     */
    public String getCaisse() {
        return caisse;
    }

    /**
     * @param caisse the caisse to set
     */
    public void setCaisse(String caisse) {
        this.caisse = caisse;
    }

    /**
     * @return the particularite
     */
    public String getParticularite() {
        return particularite;
    }

    /**
     * @param particularite the particularite to set
     */
    public void setParticularite(String particularite) {
        this.particularite = particularite;
    }

    /**
     * @return the med_traitant
     */
    public String getMed_traitant() {
        return med_traitant;
    }

    /**
     * @param med_traitant the med_traitant to set
     */
    public void setMed_traitant(String med_traitant) {
        this.med_traitant = med_traitant;
    }

    /**
     * @return the correspondant
     */
    public String getCorrespondant() {
        return correspondant;
    }

    /**
     * @param correspondant the correspondant to set
     */
    public void setCorrespondant(String correspondant) {
        this.correspondant = correspondant;
    }

    /**
     * @return the mots_cles
     */
    public String getMots_cles() {
        return mots_cles;
    }

    /**
     * @param mots_cles the mots_cles to set
     */
    public void setMots_cles(String mots_cles) {
        this.mots_cles = mots_cles;
    }

    /**
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return the groupesanguin
     */
    public String getGroupesanguin() {
        return groupesanguin;
    }

    /**
     * @param groupesanguin the groupesanguin to set
     */
    public void setGroupesanguin(String groupesanguin) {
        this.groupesanguin = groupesanguin;
    }

    
}
