/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "NOTIF_USER", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotifUser.findAll", query = "SELECT n FROM NotifUser n")})
public class NotifUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotifUserPK notifUserPK;
    @Column(name = "DATE_VUE")
    @Temporal(TemporalType.DATE)
    private Date dateVue;
    @JoinColumn(name = "NOTIF_ID", referencedColumnName = "NOTIF_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Notif notif;
    @JoinColumn(name = "USER_LOGIN", referencedColumnName = "USER_LOGIN", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AppUser appUser;

    public NotifUser() {
    }

    public NotifUser(NotifUserPK notifUserPK) {
        this.notifUserPK = notifUserPK;
    }

    public NotifUser(long notifId, String userLogin) {
        this.notifUserPK = new NotifUserPK(notifId, userLogin);
    }

    public NotifUserPK getNotifUserPK() {
        return notifUserPK;
    }

    public void setNotifUserPK(NotifUserPK notifUserPK) {
        this.notifUserPK = notifUserPK;
    }

    public Date getDateVue() {
        return dateVue;
    }

    public void setDateVue(Date dateVue) {
        this.dateVue = dateVue;
    }

    public Notif getNotif() {
        return notif;
    }

    public void setNotif(Notif notif) {
        this.notif = notif;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notifUserPK != null ? notifUserPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotifUser)) {
            return false;
        }
        NotifUser other = (NotifUser) object;
        if ((this.notifUserPK == null && other.notifUserPK != null) || (this.notifUserPK != null && !this.notifUserPK.equals(other.notifUserPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + notifUserPK;
    }
    
}
