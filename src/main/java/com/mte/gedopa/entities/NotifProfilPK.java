/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abalo
 */
@Embeddable
public class NotifProfilPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOTIF_ID")
    private long notifId;
    @Basic(optional = false)
    @NotNull
    @Size(max = 20)
    @Column(name = "PROFIL_CODE")
    private String profilCode;

    public NotifProfilPK() {
    }

    public NotifProfilPK(long notifId, String profilCode) {
        this.notifId = notifId;
        this.profilCode = profilCode;
    }

    public long getNotifId() {
        return notifId;
    }

    public void setNotifId(long notifId) {
        this.notifId = notifId;
    }

    public String getProfilCode() {
        return profilCode;
    }

    public void setProfilCode(String profilCode) {
        this.profilCode = profilCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) notifId;
        hash += (profilCode != null ? profilCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotifProfilPK)) {
            return false;
        }
        NotifProfilPK other = (NotifProfilPK) object;
        if (this.notifId != other.notifId) {
            return false;
        }
        if ((this.profilCode == null && other.profilCode != null) || (this.profilCode != null && !this.profilCode.equals(other.profilCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: notifId = " + notifId + " :: profilCode=" + profilCode;
    }
    
}
