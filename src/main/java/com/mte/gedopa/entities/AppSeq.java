/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "APP_SEQ", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AppSeq.findAll", query = "SELECT a FROM AppSeq a")})
public class AppSeq implements Serializable {
    private static final Long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SEQ_NAME")
    private String seqName;
    @Basic(optional = false)
    @Column(name = "SEQ_VAL")
    private Long seqVal;
    @Column(name = "SEQ_PREF")
    private String seqPref;
    @Basic(optional = false)
    @Column(name = "PATTERN_PREF")
    private Boolean patternPref;
    @Column(name = "SEQ_SUF")
    private String seqSuf;
    @Basic(optional = false)
    @Column(name = "PATTERN_SUF")
    private Boolean patternSuf;
    @Basic(optional = false)
    @Column(name = "SEQ_LENGTH")
    private BigInteger seqLength;

    public AppSeq() {
    }

    public AppSeq(String seqName) {
        this.seqName = seqName;
    }

    public AppSeq(String seqName, Long seqVal, Boolean patternPref, Boolean patternSuf, BigInteger seqLength) {
        this.seqName = seqName;
        this.seqVal = seqVal;
        this.patternPref = patternPref;
        this.patternSuf = patternSuf;
        this.seqLength = seqLength;
    }

    public String getSeqName() {
        return seqName;
    }

    public void setSeqName(String seqName) {
        this.seqName = seqName;
    }

    public Long getSeqVal() {
        return seqVal;
    }

    public void setSeqVal(Long seqVal) {
        this.seqVal = seqVal;
    }

    public String getSeqPref() {
        return seqPref;
    }

    public void setSeqPref(String seqPref) {
        this.seqPref = seqPref;
    }

    public Boolean getPatternPref() {
        return patternPref;
    }

    public void setPatternPref(Boolean patternPref) {
        this.patternPref = patternPref;
    }

    public String getSeqSuf() {
        return seqSuf;
    }

    public void setSeqSuf(String seqSuf) {
        this.seqSuf = seqSuf;
    }

    public Boolean getPatternSuf() {
        return patternSuf;
    }

    public void setPatternSuf(Boolean patternSuf) {
        this.patternSuf = patternSuf;
    }

    public BigInteger getSeqLength() {
        return seqLength;
    }

    public void setSeqLength(BigInteger seqLength) {
        this.seqLength = seqLength;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seqName != null ? seqName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppSeq)) {
            return false;
        }
        AppSeq other = (AppSeq) object;
        if ((this.seqName == null && other.seqName != null) || (this.seqName != null && !this.seqName.equals(other.seqName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
         return getClass().getSimpleName() + " :: ID = " + seqName;
    }
    
}
