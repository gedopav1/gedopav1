/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "REPORT_TYPE", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportType.findAll", query = "SELECT r FROM ReportType r")})
public class ReportType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(max = 30)
    @Column(name = "REP_TYPE_CODE")
    private String repTypeCode;
    @Size(max = 60)
    @Column(name = "REP_TYPE_DESC")
    private String repTypeDesc;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "repTypeCode")
    private List<Report> reportList;

    public ReportType() {
    }

    public ReportType(String repTypeCode) {
        this.repTypeCode = repTypeCode;
    }

    public String getRepTypeCode() {
        return repTypeCode;
    }

    public void setRepTypeCode(String repTypeCode) {
        this.repTypeCode = repTypeCode;
    }

    public String getRepTypeDesc() {
        return repTypeDesc;
    }

    public void setRepTypeDesc(String repTypeDesc) {
        this.repTypeDesc = repTypeDesc;
    }

    @XmlTransient
    public List<Report> getReportList() {
        return reportList;
    }

    public void setReportList(List<Report> reportList) {
        this.reportList = reportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repTypeCode != null ? repTypeCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportType)) {
            return false;
        }
        ReportType other = (ReportType) object;
        if ((this.repTypeCode == null && other.repTypeCode != null) || (this.repTypeCode != null && !this.repTypeCode.equals(other.repTypeCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = "  + repTypeCode;
    }
    
}
