/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abalo
 */
@Embeddable
public class DroitUserPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(max = 10)
    @Column(name = "ACTION_CODE")
    private String actionCode;
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "USER_LOGIN")
    private String userLogin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SCREEN_ID")
    private int screenId;

    public DroitUserPK() {
    }

    public DroitUserPK(String actionCode, String userLogin, int screenId) {
        this.actionCode = actionCode;
        this.userLogin = userLogin;
        this.screenId = screenId;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public int getScreenId() {
        return screenId;
    }

    public void setScreenId(int screenId) {
        this.screenId = screenId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actionCode != null ? actionCode.hashCode() : 0);
        hash += (userLogin != null ? userLogin.hashCode() : 0);
        hash += (int) screenId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DroitUserPK)) {
            return false;
        }
        DroitUserPK other = (DroitUserPK) object;
        if ((this.actionCode == null && other.actionCode != null) || (this.actionCode != null && !this.actionCode.equals(other.actionCode))) {
            return false;
        }
        if ((this.userLogin == null && other.userLogin != null) || (this.userLogin != null && !this.userLogin.equals(other.userLogin))) {
            return false;
        }
        if (this.screenId != other.screenId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: actionCode = " + actionCode + " :: userLogin = " + userLogin + " :: screenId = " + screenId;
    }
    
}
