/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "MENU", schema = "PUBLIC")
@SequenceGenerator(name="S_MENU", allocationSize = 1, schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m")})
public class Menu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="S_MENU")
    @Basic(optional = false)
    @Column(name = "MENU_ID")
    private Integer menuId;
    @Basic(optional = false)
    @Column(name = "MENU_DESC")
    private String menuDesc;
    @Column(name = "MENU_QTIP")
    private String menuQtip;
    @Column(name = "ICON_CLS")
    private String iconCls;
    @Column(name = "NUM_ORDRE")
    private BigInteger numOrdre;
    @Column(name = "MENU_KEY")
    private String menuKey;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "menu")
    private List<ProfilMenu> profilMenuList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "menu")
    private List<UserMenu> userMenuList;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "SCREEN_ID")
    @ManyToOne(optional = false)
    private Screen screenId;
    @JoinColumn(name = "POSITION_CODE", referencedColumnName = "POSITION_CODE")
    @ManyToOne(optional = false)
    private Position positionCode;
    @OneToMany(mappedBy = "menuParent")
    private List<Menu> menuList;
    @JoinColumn(name = "MENU_PARENT", referencedColumnName = "MENU_ID")
    @ManyToOne
    private Menu menuParent;
    @JoinColumn(name = "APP_ID", referencedColumnName = "APP_ID")
    @ManyToOne(optional = false)
    private App appId;

    public Menu() {
    }

    public Menu(Integer menuId) {
        this.menuId = menuId;
    }

    public Menu(Integer menuId, String menuDesc, Boolean actif) {
        this.menuId = menuId;
        this.menuDesc = menuDesc;
        this.actif = actif;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuDesc() {
        return menuDesc;
    }

    public void setMenuDesc(String menuDesc) {
        this.menuDesc = menuDesc;
    }

    public String getMenuQtip() {
        return menuQtip;
    }

    public void setMenuQtip(String menuQtip) {
        this.menuQtip = menuQtip;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

    public BigInteger getNumOrdre() {
        return numOrdre;
    }

    public void setNumOrdre(BigInteger numOrdre) {
        this.numOrdre = numOrdre;
    }

    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    @XmlTransient
    public List<ProfilMenu> getProfilMenuList() {
        return profilMenuList;
    }

    public void setProfilMenuList(List<ProfilMenu> profilMenuList) {
        this.profilMenuList = profilMenuList;
    }

    @XmlTransient
    public List<UserMenu> getUserMenuList() {
        return userMenuList;
    }

    public void setUserMenuList(List<UserMenu> userMenuList) {
        this.userMenuList = userMenuList;
    }

    public Screen getScreenId() {
        return screenId;
    }

    public void setScreenId(Screen screenId) {
        this.screenId = screenId;
    }

    public Position getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(Position positionCode) {
        this.positionCode = positionCode;
    }

    @XmlTransient
    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public Menu getMenuParent() {
        return menuParent;
    }

    public void setMenuParent(Menu menuParent) {
        this.menuParent = menuParent;
    }

    public App getAppId() {
        return appId;
    }

    public void setAppId(App appId) {
        this.appId = appId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuId != null ? menuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.menuId == null && other.menuId != null) || (this.menuId != null && !this.menuId.equals(other.menuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + menuId + " :: LIB = " + menuDesc;
    }
    
}
