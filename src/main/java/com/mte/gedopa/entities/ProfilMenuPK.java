/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abalo
 */
@Embeddable
public class ProfilMenuPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "MENU_ID")
    private int menuId;
    @Basic(optional = false)
    @NotNull
    @Size(max = 20)
    @Column(name = "PROFIL_CODE")
    private String profilCode;

    public ProfilMenuPK() {
    }

    public ProfilMenuPK(int menuId, String profilCode) {
        this.menuId = menuId;
        this.profilCode = profilCode;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getProfilCode() {
        return profilCode;
    }

    public void setProfilCode(String profilCode) {
        this.profilCode = profilCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) menuId;
        hash += (profilCode != null ? profilCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilMenuPK)) {
            return false;
        }
        ProfilMenuPK other = (ProfilMenuPK) object;
        if (this.menuId != other.menuId) {
            return false;
        }
        if ((this.profilCode == null && other.profilCode != null) || (this.profilCode != null && !this.profilCode.equals(other.profilCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: menuId = " + menuId + " :: profilCode=" + profilCode;
    }
    
}
