/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author abalo
 */
@Embeddable
public class NotifUserPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOTIF_ID")
    private long notifId;
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "USER_LOGIN")
    private String userLogin;

    public NotifUserPK() {
    }

    public NotifUserPK(long notifId, String userLogin) {
        this.notifId = notifId;
        this.userLogin = userLogin;
    }

    public long getNotifId() {
        return notifId;
    }

    public void setNotifId(long notifId) {
        this.notifId = notifId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) notifId;
        hash += (userLogin != null ? userLogin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotifUserPK)) {
            return false;
        }
        NotifUserPK other = (NotifUserPK) object;
        if (this.notifId != other.notifId) {
            return false;
        }
        if ((this.userLogin == null && other.userLogin != null) || (this.userLogin != null && !this.userLogin.equals(other.userLogin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: notifId = " + notifId + " :: userLogin = " + userLogin;
    }
    
}
