/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "NOTIF_PROFIL", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotifProfil.findAll", query = "SELECT n FROM NotifProfil n")})
public class NotifProfil implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotifProfilPK notifProfilPK;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "PROFIL_CODE", referencedColumnName = "PROFIL_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profil profil;
    @JoinColumn(name = "NOTIF_ID", referencedColumnName = "NOTIF_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Notif notif;

    public NotifProfil() {
    }

    public NotifProfil(NotifProfilPK notifProfilPK) {
        this.notifProfilPK = notifProfilPK;
    }

    public NotifProfil(NotifProfilPK notifProfilPK, Boolean actif) {
        this.notifProfilPK = notifProfilPK;
        this.actif = actif;
    }

    public NotifProfil(long notifId, String profilCode) {
        this.notifProfilPK = new NotifProfilPK(notifId, profilCode);
    }

    public NotifProfilPK getNotifProfilPK() {
        return notifProfilPK;
    }

    public void setNotifProfilPK(NotifProfilPK notifProfilPK) {
        this.notifProfilPK = notifProfilPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Notif getNotif() {
        return notif;
    }

    public void setNotif(Notif notif) {
        this.notif = notif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notifProfilPK != null ? notifProfilPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotifProfil)) {
            return false;
        }
        NotifProfil other = (NotifProfil) object;
        if ((this.notifProfilPK == null && other.notifProfilPK != null) || (this.notifProfilPK != null && !this.notifProfilPK.equals(other.notifProfilPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + notifProfilPK + " ]";
    }
    
}
