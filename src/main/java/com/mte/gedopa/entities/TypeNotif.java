/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "TYPE_NOTIF", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeNotif.findAll", query = "SELECT t FROM TypeNotif t")})
public class TypeNotif implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(max = 15)
    @Column(name = "TYPE_NOTIF_CODE")
    private String typeNotifCode;
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "TYPE_NOTIF_LIB")
    private String typeNotifLib;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "typeNotifCode")
    private List<Notif> notifList;

    public TypeNotif() {
    }

    public TypeNotif(String typeNotifCode) {
        this.typeNotifCode = typeNotifCode;
    }

    public TypeNotif(String typeNotifCode, String typeNotifLib) {
        this.typeNotifCode = typeNotifCode;
        this.typeNotifLib = typeNotifLib;
    }

    public String getTypeNotifCode() {
        return typeNotifCode;
    }

    public void setTypeNotifCode(String typeNotifCode) {
        this.typeNotifCode = typeNotifCode;
    }

    public String getTypeNotifLib() {
        return typeNotifLib;
    }

    public void setTypeNotifLib(String typeNotifLib) {
        this.typeNotifLib = typeNotifLib;
    }

    @XmlTransient
    public List<Notif> getNotifList() {
        return notifList;
    }

    public void setNotifList(List<Notif> notifList) {
        this.notifList = notifList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typeNotifCode != null ? typeNotifCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeNotif)) {
            return false;
        }
        TypeNotif other = (TypeNotif) object;
        if ((this.typeNotifCode == null && other.typeNotifCode != null) || (this.typeNotifCode != null && !this.typeNotifCode.equals(other.typeNotifCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + typeNotifCode + " :: LIB = " + typeNotifLib;
    }
    
}
