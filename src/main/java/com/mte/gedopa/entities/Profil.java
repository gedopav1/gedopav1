/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "PROFIL", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profil.findAll", query = "SELECT p FROM Profil p")})
public class Profil implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Size(max = 20)
    @Column(name = "PROFIL_CODE")
    private String profilCode;
    @Basic(optional = false)
    @Size(max = 100)
    @Column(name = "PROFIL_DESC")
    private String profilDesc;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "profil")
    private List<ProfilMenu> profilMenuList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "profilCode")
    private List<AppUser> appUserList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "profil")
    private List<NotifProfil> notifProfilList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "profil")
    private List<DroitProfil> droitProfilList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "profil")
    private List<ProfilReport> profilReportList;

    public Profil() {
    }

    public Profil(String profilCode) {
        this.profilCode = profilCode;
    }

    public Profil(String profilCode, String profilDesc, Boolean actif) {
        this.profilCode = profilCode;
        this.profilDesc = profilDesc;
        this.actif = actif;
    }

    public String getProfilCode() {
        return profilCode;
    }

    public void setProfilCode(String profilCode) {
        this.profilCode = profilCode;
    }

    public String getProfilDesc() {
        return profilDesc;
    }

    public void setProfilDesc(String profilDesc) {
        this.profilDesc = profilDesc;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    @XmlTransient
    public List<ProfilMenu> getProfilMenuList() {
        return profilMenuList;
    }

    public void setProfilMenuList(List<ProfilMenu> profilMenuList) {
        this.profilMenuList = profilMenuList;
    }

    @XmlTransient
    public List<AppUser> getAppUserList() {
        return appUserList;
    }

    public void setAppUserList(List<AppUser> appUserList) {
        this.appUserList = appUserList;
    }

    @XmlTransient
    public List<NotifProfil> getNotifProfilList() {
        return notifProfilList;
    }

    public void setNotifProfilList(List<NotifProfil> notifProfilList) {
        this.notifProfilList = notifProfilList;
    }

    @XmlTransient
    public List<DroitProfil> getDroitProfilList() {
        return droitProfilList;
    }

    public void setDroitProfilList(List<DroitProfil> droitProfilList) {
        this.droitProfilList = droitProfilList;
    }

    @XmlTransient
    public List<ProfilReport> getProfilReportList() {
        return profilReportList;
    }

    public void setProfilReportList(List<ProfilReport> profilReportList) {
        this.profilReportList = profilReportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profilCode != null ? profilCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profil)) {
            return false;
        }
        Profil other = (Profil) object;
        if ((this.profilCode == null && other.profilCode != null) || (this.profilCode != null && !this.profilCode.equals(other.profilCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = "+ profilCode;
    }
    
}
