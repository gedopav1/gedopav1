/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "ANTECEDENT")
@SequenceGenerator(name="antecedent_idantecedent_seq", allocationSize = 1)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Antecedent.findAll", query = "SELECT a FROM Antecedent a")})
public class Antecedent implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="antecedent_idantecedent_seq")
    @Basic(optional = false)
    @Column(name = "idantecedent")
    private Integer idantecedent;
    @Column(name = "descantecedent")
    private String descantecedent;
     
    public Antecedent() {
    }

    public Antecedent(Integer idantecedent) {
        this.idantecedent = idantecedent;
    }

    public Antecedent(Integer idantecedent, String descantecedent) {
        this.idantecedent = idantecedent;
        this.descantecedent = descantecedent;
   }


    @Override
    public String toString() {
       return getClass().getSimpleName() + " :: ID = " + getIdantecedent();
    }   

    /**
     * @return the idantecedent
     */
    public Integer getIdantecedent() {
        return idantecedent;
    }

    /**
     * @param idantecedent the idantecedent to set
     */
    public void setIdantecedent(Integer idantecedent) {
        this.idantecedent = idantecedent;
    }

    /**
     * @return the descantecedent
     */
    public String getDescantecedent() {
        return descantecedent;
    }

    /**
     * @param descantecedent the descantecedent to set
     */
    public void setDescantecedent(String descantecedent) {
        this.descantecedent = descantecedent;
    }
    
   
}
