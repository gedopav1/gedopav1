/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author abalo
 */
@Embeddable
public class ReportParamPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "REPORT_ID")
    private int reportId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PARAM_ID")
    private int paramId;

    public ReportParamPK() {
    }

    public ReportParamPK(int reportId, int paramId) {
        this.reportId = reportId;
        this.paramId = paramId;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public int getParamId() {
        return paramId;
    }

    public void setParamId(int paramId) {
        this.paramId = paramId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) reportId;
        hash += (int) paramId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportParamPK)) {
            return false;
        }
        ReportParamPK other = (ReportParamPK) object;
        if (this.reportId != other.reportId) {
            return false;
        }
        if (this.paramId != other.paramId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: reportId = " + reportId + " :: paramId = " + paramId;
    }
    
}
