/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "HYPOTHESEDIAG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hypothesediag.findAll", query = "SELECT a FROM Hypothesediag a")})
public class Hypothesediag implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idhypo")
    private String idhypo;
    @Column(name = "libhypo")
    private String libhypo;
     
    public Hypothesediag() {
    }

    public Hypothesediag(String idhypo) {
        this.idhypo = idhypo;
    }

    public Hypothesediag(String idhypo, String libhypo) {
        this.idhypo = idhypo;
        this.libhypo = libhypo;
   }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdhypo()!= null ? getIdhypo().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hypothesediag)) {
            return false;
        }
        Hypothesediag other = (Hypothesediag) object;
        if ((this.getIdhypo() == null && other.getIdhypo() != null) || (this.getIdhypo() != null && !this.idhypo.equals(other.idhypo))) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getIdhypo() + " :: LIB = " + getLibhypo();
    }
     /**
     * @return the idhypo
     */
    public String getIdhypo() {
        return idhypo;
    }

    /**
     * @param idhypo the idhypo to set
     */
    public void setIdhypo(String idhypo) {
        this.idhypo = idhypo;
    }

    /**
     * @return the libhypo
     */
    public String getLibhypo() {
        return libhypo;
    }

    /**
     * @param libhypo the libhypo to set
     */
    public void setLibhypo(String libhypo) {
        this.libhypo = libhypo;
    }

   
}
