/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abalo
 */
@Embeddable
public class AnalyseDemandeePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idconsultaion")
    private long idconsultaion;
    @Basic(optional = false)
    @Column(name = "idanamed")
    private String idanamed;

    public AnalyseDemandeePK(String idanamed, long idconsultaion) {
        this.idconsultaion = idconsultaion;
        this.idanamed = idanamed;
    }
    
    public AnalyseDemandeePK() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdanamed() != null ? getIdanamed().hashCode() : 0);
        hash += (int) getIdconsultaion();
        return hash;
    }

   /* @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalyseDemandeePK)) {
            return false;
        }
        AnalyseDemandeePK other = (AnalyseDemandeePK) object;
        if (this.idanamed != other.idanamed) {
            return false;
        }
        if (this.idconsultaion != other.idconsultaion) {
            return false;
        }
        return true;
    }
*/
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalyseDemandeePK)) {
            return false;
        }
        AnalyseDemandeePK other = (AnalyseDemandeePK) object;
        if ((this.getIdanamed() == null && other.getIdanamed() != null) || (this.getIdanamed() != null && !this.idanamed.equals(other.idanamed))) {
            return false;
        }
        if (this.getIdconsultaion() != other.getIdconsultaion()) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: idanamed = " + idanamed + " :: idconsultaion = " + idconsultaion;
    }
    /**
     * @return the idconsultaion
     */
    public long getIdconsultaion() {
        return idconsultaion;
    }

    /**
     * @param idconsultaion the idconsultaion to set
     */
    public void setIdconsultaion(long idconsultaion) {
        this.idconsultaion = idconsultaion;
    }

    /**
     * @return the idanamed
     */
    public String getIdanamed() {
        return idanamed;
    }

    /**
     * @param idanamed the idanamed to set
     */
    public void setIdanamed(String idanamed) {
        this.idanamed = idanamed;
    }
    
}
