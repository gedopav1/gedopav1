/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "PARAM", schema = "PUBLIC")
@SequenceGenerator(name="S_PARAM", allocationSize = 1, schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Param.findAll", query = "SELECT p FROM Param p")})
public class Param implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="S_PARAM")
    @Basic(optional = false)
    @Column(name = "PARAM_ID")
    private Integer paramId;
    @Size(max = 100)
    @Column(name = "PARAM_CODE")
    private String paramCode;
    @Size(max = 200)
    @Column(name = "PARAM_DESC")
    private String paramDesc;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "param")
    private List<ReportParam> reportParamList;

    public Param() {
    }

    public Param(Integer paramId) {
        this.paramId = paramId;
    }

    public Integer getParamId() {
        return paramId;
    }

    public void setParamId(Integer paramId) {
        this.paramId = paramId;
    }

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public String getParamDesc() {
        return paramDesc;
    }

    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc;
    }

    @XmlTransient
    public List<ReportParam> getReportParamList() {
        return reportParamList;
    }

    public void setReportParamList(List<ReportParam> reportParamList) {
        this.reportParamList = reportParamList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paramId != null ? paramId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Param)) {
            return false;
        }
        Param other = (Param) object;
        if ((this.paramId == null && other.paramId != null) || (this.paramId != null && !this.paramId.equals(other.paramId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + paramId;
    }
    
}
