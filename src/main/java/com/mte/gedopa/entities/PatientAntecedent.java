/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abalo
 */
@Entity
@Table(name = "patient_antecedent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PatientAntecedent.findAll", query = "SELECT f FROM PatientAntecedent f")})
public class PatientAntecedent implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private PatientAntecedentPK patientAntecedentPK;
    //@Basic(optional = false)
    @Column(name = "datedebutant")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datedebutant;
   // @Basic(optional = false)
    @Column(name = "datefinant")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datefinant;
    //@Basic(optional = false)
    @Column(name = "commentaires")
    private String commentaires;
    
    @JoinColumn(name = "idantecedent", referencedColumnName = "idantecedent", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Antecedent antecedent;
    @JoinColumn(name = "id_patient", referencedColumnName = "id_patient", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Patient patient;

    public PatientAntecedent() {
    }

    public PatientAntecedent(PatientAntecedentPK patientAntecedentPK) {
        this.patientAntecedentPK = patientAntecedentPK;
    }

    public PatientAntecedent(PatientAntecedentPK patientAntecedentPK, Date datedebutant, Date datefinant, String commentaires) {
        this.patientAntecedentPK = patientAntecedentPK;
        this.datedebutant = datedebutant;
        this.datefinant = datefinant;
        this.commentaires = commentaires;
    }
    
    
    public PatientAntecedent(int idantecedent, long id_patient) {
        this.patientAntecedentPK = new PatientAntecedentPK(idantecedent, id_patient);
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getPatientAntecedentPK() != null ? getPatientAntecedentPK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PatientAntecedent)) {
            return false;
        }
        PatientAntecedent other = (PatientAntecedent) object;
        if ((this.getPatientAntecedentPK() == null && other.getPatientAntecedentPK() != null) || (this.getPatientAntecedentPK() != null && !this.patientAntecedentPK.equals(other.patientAntecedentPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getPatientAntecedentPK();
    }

    /**
     * @return the patientAntecedentPK
     */
    public PatientAntecedentPK getPatientAntecedentPK() {
        return patientAntecedentPK;
    }

    /**
     * @param patientAntecedentPK the patientAntecedentPK to set
     */
    public void setPatientAntecedentPK(PatientAntecedentPK patientAntecedentPK) {
        this.patientAntecedentPK = patientAntecedentPK;
    }

    /**
     * @return the datedebutant
     */
    public Date getDatedebutant() {
        return datedebutant;
    }

    /**
     * @param datedebutant the datedebutant to set
     */
    public void setDatedebutant(Date datedebutant) {
        this.datedebutant = datedebutant;
    }

    /**
     * @return the datefinant
     */
    public Date getDatefinant() {
        return datefinant;
    }

    /**
     * @param datefinant the datefinant to set
     */
    public void setDatefinant(Date datefinant) {
        this.datefinant = datefinant;
    }

    /**
     * @return the commentaires
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     * @param commentaires the commentaires to set
     */
    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     * @return the antecedent
     */
    public Antecedent getAntecedent() {
        return antecedent;
    }

    /**
     * @param antecedent the antecedent to set
     */
    public void setAntecedent(Antecedent antecedent) {
        this.antecedent = antecedent;
    }

    /**
     * @return the patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }
    
}
