
package com.mte.gedopa.view.mbeans.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
 
/**
 *
 * @author abalo
 */
@ManagedBean(name = "requestBean")
@RequestScoped
public class RequestBean implements Serializable{
    
    private Date sysDate;
    
    public RequestBean(){
        sysDate = Calendar.getInstance().getTime();
    }

    /**
     * @return the sysDate
     */
    public Date getSysDate() {
        return sysDate;
    }

    /**
     * @param sysDate the sysDate to set
     */
    public void setSysDate(Date sysDate) {
        this.sysDate = sysDate;
    }
    
}
