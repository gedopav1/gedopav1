/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.mbeans.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 * @author Raph
 */
public class Formatage {

    static DecimalFormat form;

    public static String montant(BigDecimal mtt) {
        form = new DecimalFormat("#,###");
        return form.format(mtt);
    }
}
