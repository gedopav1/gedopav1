
package com.mte.gedopa.view.mbeans.gedopa;

import com.mte.gedopa.util.OperateurSql;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import com.mte.gedopa.service.gedopa.AnalyseDemandeeService;
import com.mte.gedopa.service.impl.gedopa.AnalyseDemandeeServiceImpl;
import com.mte.gedopa.entities.AnalyseDemandee;
import com.mte.gedopa.entities.AnalyseDemandeePK;
import com.mte.gedopa.util.MessageProvider;
import java.text.MessageFormat;
import java.util.ArrayList;
import com.mte.gedopa.service.gedopa.ConsultationService;
import com.mte.gedopa.service.impl.gedopa.ConsultationServiceImpl;
import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.service.gedopa.AnalysemedService;
import com.mte.gedopa.service.impl.gedopa.AnalysemedServiceImpl;
import com.mte.gedopa.entities.Analysemed;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import com.mte.gedopa.view.mbeans.util.LoginBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author ABALO Kokou
 */
@ManagedBean(name="analyseDemandeeBean")
@ViewScoped
public class AnalyseDemandeeBean implements Serializable{
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    private AnalyseDemandeeService serviceDetail;
    private ConsultationService consultationService;//////// A supprimer ////////////
    private AnalysemedService analysemedService;
    
    private int indexDetail;
    private AnalyseDemandee formObjectDetail;
    private AnalyseDemandee formObjectDetailTmp;
    private AnalyseDemandee selectedObjectDetail;
    private List<AnalyseDemandee> dataListDetail = new ArrayList<>();
    private List<Consultation> consultationList;
    private List<Analysemed> medicamentList;
    private List<AnalyseDemandee> filteredListDetail;
    private String message;
    private boolean editModeDetail;
    
    private String gridEmptyMessageDetail = "";
    
    private List<AnalyseDemandee> toAddList = new ArrayList<>();
    private int idAnalyseDemandeeProvisoire;
    private final String codeAnalysemed = "";
    
    
    @PostConstruct
    public void init() {
        try {
            serviceDetail = AnalyseDemandeeServiceImpl.getInstance();
            consultationService = ConsultationServiceImpl.getInstance();
            analysemedService = AnalysemedServiceImpl.getInstance();
            gridEmptyMessageDetail = getI18nMessage("msg.grid.load.empty");
            setAnalysemedList(getAnalysemedService().read());
            effacerDetail();
            effacerList();
        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public void validerDetail() {
        if(isEditModeDetail()) { try {
            System.out.println("// Mode modif");
                   // Mode modif
                   //si c'est une ligne qui n'existe pas encore ds la BD
                if (toAddList.contains(formObjectDetail)) {
                    toAddList.set(toAddList.indexOf(formObjectDetail), formObjectDetail);
                    dataListDetail.set(indexDetail, formObjectDetail);
                } else {//sinon on modifie directement la ligne existante ds la BD
                    getServiceDetail().update(getFormObjectDetail(), getLoginBean().getConnectedUser(), getFormObjectDetail().toString());
                    dataListDetail.set(indexDetail, formObjectDetail);
                    setMessage(getI18nMessage("msg.action.update.success", formObjectDetail.getAnalyseDemandeePK()));
                    showSuccesOppDialog();
                 }
                 effacerDetail();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.update.fail", formObjectDetail.getAnalyseDemandeePK(), ex.getMessage()));
                Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
        else {
            //Mode ajout -- ok
            try {
                formObjectDetail.setAnalyseDemandeePK(new AnalyseDemandeePK(formObjectDetail.getAnalysemed().getIdanamed(), 0));
                AnalyseDemandee entity = formObjectDetail;
                
                if (!toAddList.contains(formObjectDetail)) {
                      getDataListDetail().add(entity);
                      toAddList.add(entity);
                }
                effacerDetail();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.create.fail", formObjectDetail.getAnalyseDemandeePK(), ex.getMessage()));
                Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }
    
    public void effacerDetail() {
        setFormObjectDetail(new AnalyseDemandee(new AnalyseDemandeePK(codeAnalysemed,0)));
        setSelectedObjectDetail(null);
        setEditModeDetail(false);
    }
    
     public void effacerList() {
        dataListDetail.clear();
        toAddList.clear();
     }
     public void rowSelectDetail() {
         try {
            setFormObjectDetail(serviceDetail.getDao().clone(getSelectedObjectDetail()));
            
         } catch (Exception ex) {
            Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, null, ex);
        }
            indexDetail = dataListDetail.indexOf(selectedObjectDetail);
            setEditModeDetail(true);
    }
    
    public void supprimerDetail() {
           if(selectedObjectDetail == null) {
            setMessage(getI18nMessage("msg.action.delete.error.noselect"));
           } else {
             try {
                    AnalyseDemandeePK id = selectedObjectDetail.getAnalyseDemandeePK();
                   // medicamentList.add(dataListDetail.get(indexDetail).getAnalysemed());
           
                    AnalyseDemandee entity = serviceDetail.findByPk(id);
                    if (entity != null) {
                        serviceDetail.delete(entity, getLoginBean().getConnectedUser(), entity.toString());
                    }
                    //
                    dataListDetail.remove(indexDetail);
                    toAddList.remove(selectedObjectDetail);
                    effacerDetail();
                    setMessage(getI18nMessage("msg.action.delete.success", id));
                    showSuccesOppDialog();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.delete.fail", selectedObjectDetail.getAnalyseDemandeePK(), ex.getMessage()));
                Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
        
    }
    
    public void chargerDetailFrnt(Integer master) { // meme fonction que chargerDetailDmd
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("dmdFrntPrest.idDmdFrntPrest", OperateurCond.EQUAL, master));
                
        dataListDetail.clear();
        try {
            dataListDetail.addAll(serviceDetail.findBy(criteres));
             if(dataListDetail.isEmpty()){
                gridEmptyMessageDetail = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
     }
    
     public void chargerDetailDmd(Consultation operManutConc) {
        dataListDetail.clear();
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("consultation.idconsultaion", OperateurCond.EQUAL, operManutConc.getIdconsultaion()));
        try {
            dataListDetail.addAll(serviceDetail.findBy(criteres));
             if(dataListDetail.isEmpty()){
                gridEmptyMessageDetail = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
     }
     

    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDeleteDetFrnt').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOppDetFrnt').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOppDetFrnt').show();");
    }

    public void rechercher(boolean searchByAnd) {
        Map<String,Object> params = new HashMap<>();
        if(formObjectDetail.getConsultation() != null && formObjectDetail.getConsultation().getIdconsultaion()!= null ) {
            params.put("consultation.idconsultaion", formObjectDetail.getConsultation().getIdconsultaion());
        }
        
        if(formObjectDetail.getAnalysemed() != null && formObjectDetail.getAnalysemed().getIdanamed()!= null ) {
            params.put("Analysemed.codemedi", formObjectDetail.getAnalysemed().getIdanamed());
        }
        
        if(formObjectDetail.getResultatana() != null ) {
            params.put("resultatana", formObjectDetail.getResultatana());
        }
        dataListDetail.clear();
        try {
            dataListDetail.addAll(serviceDetail.findBy(params, (searchByAnd ? OperateurSql.AND : OperateurSql.OR)));
            if(dataListDetail.isEmpty()){
                gridEmptyMessageDetail = getI18nMessage("msg.grid.search.empty");
            }
            
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }
    
    public void rechercherDetail(Consultation demande) {
        
        dataListDetail.clear();
        toAddList.clear();
        List<Critere> criteres = new ArrayList<>();
        if (demande != null && demande.getIdconsultaion()!= null) {
            criteres.add(new Critere("consultation.idconsultaion", OperateurCond.EQUAL, demande.getIdconsultaion()));
          try {
            dataListDetail.addAll(serviceDetail.findBy(criteres));
            if(dataListDetail.isEmpty()){
                gridEmptyMessageDetail = getI18nMessage("msg.grid.search.empty");
               }else{
                setAnalysemedList(getAnalysemedService().read());
                    if (!dataListDetail.isEmpty()) {
                    for (AnalyseDemandee det : dataListDetail) {
                           if(medicamentList.contains(det.getAnalysemed())){
                              medicamentList.remove(det.getAnalysemed());
                           }
                        }
                    }
             }
        
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(AnalyseDemandeeBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
      }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }
    
    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the serviceDetail
     */
    public AnalyseDemandeeService getServiceDetail() {
        return serviceDetail;
    }

    /**
     * @return the formObjectDetail
     */
    public AnalyseDemandee getFormObjectDetail() {
        return formObjectDetail;
    }

    /**
     * @param formObjectDetail the formObjectDetail to set
     */
    public void setFormObjectDetail(AnalyseDemandee formObjectDetail) {
        this.formObjectDetail = formObjectDetail;
    }

    /**
     * @return the selectedObjectDetail
     */
    public AnalyseDemandee getSelectedObjectDetail() {
        return selectedObjectDetail;
    }

    /**
     * @param selectedObjectDetail the selectedObjectDetail to set
     */
    public void setSelectedObjectDetail(AnalyseDemandee selectedObjectDetail) {
        this.selectedObjectDetail = selectedObjectDetail;
    }

    /**
     * @return the dataListDetail
     */
    public List<AnalyseDemandee> getDataListDetail() {
        return dataListDetail;
    }

    /**
     * @param dataListDetail the dataListDetail to set
     */
    public void setDataListDetail(List<AnalyseDemandee> dataListDetail) {
        this.dataListDetail = dataListDetail;
    }

    /**
     * @return the filteredListDetail
     */
    public List<AnalyseDemandee> getFilteredListDetail() {
        return filteredListDetail;
    }

    /**
     * @param filteredListDetail the filteredListDetail to set
     */
    public void setFilteredListDetail(List<AnalyseDemandee> filteredListDetail) {
        this.filteredListDetail = filteredListDetail;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editModeDetail
     */
    public boolean isEditModeDetail() {
        return editModeDetail;
    }

    /**
     * @param editModeDetail the editModeDetail to set
     */
    public void setEditModeDetail(boolean editModeDetail) {
        this.editModeDetail = editModeDetail;
    }

    /**
     * @return the gridEmptyMessageDetail
     */
    public String getGridEmptyMessageDetail() {
        return gridEmptyMessageDetail;
    }

    /**
     * @param gridEmptyMessageDetail the gridEmptyMessageDetail to set
     */
    public void setGridEmptyMessageDetail(String gridEmptyMessageDetail) {
        this.gridEmptyMessageDetail = gridEmptyMessageDetail;
    }

    /**
     * @return the consultationService
     */
    public ConsultationService getDmdFrntPrestService() {
        return consultationService;
    }

    /**
     * @return the dmdFrntPrestList
     */
    public List<Consultation> getDmdFrntPrestList() {
        return consultationList;
    }

    /**
     * @param dmdFrntPrestList the dmdFrntPrestList to set
     */
    public void setDmdFrntPrestList(List<Consultation> dmdFrntPrestList) {
        this.consultationList = dmdFrntPrestList;
    }
    /**
     * @return the analysemedService
     */
    public AnalysemedService getAnalysemedService() {
        return analysemedService;
    }

    /**
     * @return the medicamentList
     */
    public List<Analysemed> getAnalysemedList() {
        return medicamentList;
    }

    /**
     * @param medicamentList the medicamentList to set
     */
    public void setAnalysemedList(List<Analysemed> medicamentList) {
        this.medicamentList = medicamentList;
    }
   
    /**
     * @return the formObjectDetailTmp
     */
    public AnalyseDemandee getFormObjectDetailTmp() {
        return formObjectDetailTmp;
    }

    /**
     * @param formObjectDetailTmp the formObjectDetailTmp to set
     */
    public void setFormObjectDetailTmp(AnalyseDemandee formObjectDetailTmp) {
        this.formObjectDetailTmp = formObjectDetailTmp;
    }

    /**
     * @return the toAddList
     */
    public List<AnalyseDemandee> getToAddList() {
        return toAddList;
    }

    /**
     * @param toAddList the toAddList to set
     */
    public void setToAddList(List<AnalyseDemandee> toAddList) {
        this.toAddList = toAddList;
    }
    
      /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
