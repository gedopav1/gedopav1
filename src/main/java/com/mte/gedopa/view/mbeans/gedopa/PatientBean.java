package com.mte.gedopa.view.mbeans.gedopa;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import com.mte.gedopa.service.gedopa.PatientService;
import com.mte.gedopa.service.impl.gedopa.PatientServiceImpl;
import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.util.MessageProvider;
import java.text.MessageFormat;
import java.util.ArrayList;
import com.mte.gedopa.service.admin.AppSeqService;
import com.mte.gedopa.service.admin.SysParamService;
import com.mte.gedopa.service.impl.admin.AppSeqServiceImpl;
import com.mte.gedopa.service.impl.admin.SysParamServiceImpl;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.view.mbeans.util.LoginBean;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author H
 */
@ManagedBean(name = "patientBean")
@ViewScoped
public class PatientBean implements Serializable, PropertyChangeListener {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    private PatientService service;
    private SysParamService sps;
    private AppSeqService seqSrv;
    private int index;
    private Patient formObject;
    private Patient selectedObject;
    private List<Patient> dataList = new ArrayList<>();
    private List<Patient> filteredList;
    private String message;
    private boolean editMode;
    private String gridEmptyMessage = "";
    private String repUrl = "";
    private boolean collapseInfosNavire = true;
    private boolean collapseInfosEscale = true;
    private boolean escaleAbri = false;
    private final List<Critere> criteres = new ArrayList<>();
    PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    @PostConstruct
    public void init() {
        try {
            service = PatientServiceImpl.getInstance();
            sps = SysParamServiceImpl.getInstance();
            seqSrv = AppSeqServiceImpl.getInstance();
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");

            criteres.add(new Critere("systeme", false));
//            effacer();

        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(PatientBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createFacesMessage(String summary, String detail) {
        FacesContext fc = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage();
        fm.setSummary(summary);
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        fc.addMessage(null, fm);
        //fc.renderResponse();
    }

    private void invalidate() {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.renderResponse();
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    public void addPropertyChangeListener(final PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(final PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }
   
    /**
     * @return the service
     */
    public PatientService getService() {
        return service;
    }

    /**
     * @return the formObject
     */
    public Patient getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
 /*   public void setFormObject(Patient formObject) {
        this.formObject = formObject;
        navireChanged();
        typeEscaleChanged();
        consignataireChanged();
    }
*/
    /**
     * @return the selectedObject
     */
    public Patient getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Patient selectedObject) {
        Patient oldValue = this.selectedObject;
        this.selectedObject = selectedObject;
        pcs.firePropertyChange("selectedPatient", oldValue, selectedObject);

    }


    /**
     * @return the dataList
     */
    public List<Patient> getDataList() {

        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Patient> dataList) {
        this.dataList = dataList;
    }

    /**
     * @return the filteredList
     */
    public List<Patient> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Patient> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }
    /**
     * @return the repUrl
     */
    public String getRepUrl() {
        return repUrl;
    }

    /**
     * @param repUrl the repUrl to set
     */
    public void setRepUrl(String repUrl) {
        this.repUrl = repUrl;
    }
    /**
     * @return the collapseInfosNavire
     */
    public boolean isCollapseInfosNavire() {
        return collapseInfosNavire;
    }

    /**
     * @param collapseInfosNavire the collapseInfosNavire to set
     */
    public void setCollapseInfosNavire(boolean collapseInfosNavire) {
        this.collapseInfosNavire = collapseInfosNavire;
    }

    /**
     * @return the collapseInfosEscale
     */
    public boolean isCollapseInfosEscale() {
        return collapseInfosEscale;
    }

    /**
     * @param collapseInfosEscale the collapseInfosEscale to set
     */
    public void setCollapseInfosEscale(boolean collapseInfosEscale) {
        this.collapseInfosEscale = collapseInfosEscale;
    }

    /**
     * @return the escaleAbri
     */
    public boolean isEscaleAbri() {
        return escaleAbri;
    }

    /**
     * @param escaleAbri the escaleAbri to set
     */
    public void setEscaleAbri(boolean escaleAbri) {
        this.escaleAbri = escaleAbri;
    }
     /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
