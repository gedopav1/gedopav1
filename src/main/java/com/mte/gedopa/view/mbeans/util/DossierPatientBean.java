/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.mbeans.util;

import com.mte.gedopa.entities.AnalyseDemandee;
import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.entities.HypothesediagCons;
import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.service.admin.AppSeqService;
import com.mte.gedopa.service.admin.SysParamService;
import com.mte.gedopa.service.gedopa.AnalyseDemandeeService;
import com.mte.gedopa.service.impl.admin.AppSeqServiceImpl;
import com.mte.gedopa.service.impl.admin.SysParamServiceImpl;
import com.mte.gedopa.service.impl.gedopa.ConsultationServiceImpl;
import com.mte.gedopa.service.gedopa.ConsultationService;
import com.mte.gedopa.service.gedopa.HypothesediagConsService;
import com.mte.gedopa.service.gedopa.PrescriptionService;
import com.mte.gedopa.service.impl.gedopa.AnalyseDemandeeServiceImpl;
import com.mte.gedopa.service.impl.gedopa.HypothesediagConsServiceImpl;
import com.mte.gedopa.service.impl.gedopa.PrescriptionServiceImpl;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.MessageProvider;
import com.mte.gedopa.util.OperateurCond;
import com.mte.gedopa.util.OperateurSql;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *selectedConsultation
 * @author abalo
 */
@ManagedBean(name = "dossierPatientBean")
@ViewScoped
public class DossierPatientBean implements Serializable, PropertyChangeListener {

    @ManagedProperty(value = "#{searchPatientRecordBean}")
    private SearchPatientRecordBean searchPatientRecordBean;
    
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
  
   /* @ManagedProperty(value = "#{infoPatientBean}")
    private InfoPatientBean infoPatientBean;*/

    private HypothesediagConsService hypothesediagConsService;
    private PrescriptionService prescriptionService;
    private AnalyseDemandeeService analyseDemandeeService;
  
    private ConsultationService service;

    private int index;
    private Consultation formObject;
    private List<HypothesediagCons> dataListDiag = new ArrayList<>();
    private List<Prescription> dataListPrescription = new ArrayList<>();
    private List<AnalyseDemandee> dataListAnal = new ArrayList<>();
    
    private List<HypothesediagCons> filteredHypoListDetail;
    private List<AnalyseDemandee> filteredAnaListDetail;
    private List<Prescription> filteredPresListDetail;
    
    private HypothesediagCons selectedHypo;
    private Prescription selectedPrescription;
    private AnalyseDemandee selectedAna;
        
    private List<Consultation> filteredList;
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";
    
    PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    @PostConstruct
    public void init() {
        try {
            service = ConsultationServiceImpl.getInstance();
            hypothesediagConsService = HypothesediagConsServiceImpl.getInstance();
            prescriptionService = PrescriptionServiceImpl.getInstance();
            analyseDemandeeService = AnalyseDemandeeServiceImpl.getInstance();
            
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");

            try {
                getSearchPatientRecordBean().addPropertyChangeListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            effacer();
        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(DossierPatientBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void effacer() {
        getSearchPatientRecordBean().effacer();
     //   infoPatientBean.effacer();
        dataListAnal.clear();
        dataListPrescription.clear();
        getDataListDiag().clear();
      }

    public void rechercherDiagnostique(Long idPatient) {
        System.out.println("rechercherDiagnostique---------------------");
        List<Critere> criteres = new ArrayList<>();
        try {

            criteres.add(new Critere("consultation.id_patient.id_patient", OperateurCond.EQUAL, idPatient, OperateurSql.AND));

            getDataListDiag().clear();

            getDataListDiag().addAll(hypothesediagConsService.findBy(criteres));
            if (getDataListDiag().isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
            System.out.println(getDataListDiag());
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(DossierPatientBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }
    public void rechercherPrescription(Long idPatient) {

        List<Critere> criteres = new ArrayList<>();
        try {

            criteres.add(new Critere("consultation.id_patient.id_patient", OperateurCond.EQUAL, idPatient, OperateurSql.AND));

            dataListPrescription.clear();

            dataListPrescription.addAll(prescriptionService.findBy(criteres));
            if (dataListPrescription.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
            System.out.println(dataListPrescription);
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(DossierPatientBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }
    
    public void rechercherAnalyse(Long idPatient) {

        List<Critere> criteres = new ArrayList<>();
        try {

            criteres.add(new Critere("consultation.id_patient.id_patient", OperateurCond.EQUAL, idPatient, OperateurSql.AND));

            dataListAnal.clear();

            dataListAnal.addAll(analyseDemandeeService.findBy(criteres));
            if (dataListAnal.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
            System.out.println(dataListAnal);
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(DossierPatientBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }
    
    public void rechercherAntecedent(Long idPatient) {

        List<Critere> criteres = new ArrayList<>();
        try {

            criteres.add(new Critere("idconsultaion.id_patient", OperateurCond.EQUAL, idPatient, OperateurSql.AND));

            dataListAnal.clear();

            dataListAnal.addAll(analyseDemandeeService.findBy(criteres));
            if (dataListAnal.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
            //System.out.println(dataListPrescription);
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(DossierPatientBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }
   
    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDelete').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        if ("selectedPatient".equalsIgnoreCase(evt.getPropertyName())) {
            if (evt.getNewValue() instanceof Patient) {

                Patient patient = (Patient) evt.getNewValue();

                rechercherDiagnostique(patient.getId_patient());
                rechercherPrescription(patient.getId_patient());
                rechercherAnalyse(patient.getId_patient());
              }

        }

    }

    /**
     * @return the formObject
     */
    public Consultation getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Consultation formObject) {
        this.formObject = formObject;
    }

    public ConsultationService getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(ConsultationService service) {
        this.service = service;
    }

    /**
     * @return the filteredList
     */
    public List<Consultation> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Consultation> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }
   /**
     * @return the prescriptionService
     */
    public PrescriptionService getPrescriptionService() {
        return prescriptionService;
    }

    /**
     * @param prescriptionService the prescriptionService to set
     */
    public void setPrescriptionService(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    /**
     * @return the dataListPrescription
     */
    public List<Prescription> getDataListPrescription() {
        return dataListPrescription;
    }

    /**
     * @param dataListPrescription the dataListPrescription to set
     */
    public void setDataListPrescription(List<Prescription> dataListPrescription) {
        this.dataListPrescription = dataListPrescription;
    }

    /**
     * @return the dataListAnal
     */
    public List<AnalyseDemandee> getDataListAnal() {
        return dataListAnal;
    }

    /**
     * @param dataListAnal the dataListAnal to set
     */
    public void setDataListAnal(List<AnalyseDemandee> dataListAnal) {
        this.dataListAnal = dataListAnal;
    }

    /**
     * @return the filteredHypoListDetail
     */
    public List<HypothesediagCons> getFilteredHypoListDetail() {
        return filteredHypoListDetail;
    }

    /**
     * @param filteredHypoListDetail the filteredHypoListDetail to set
     */
    public void setFilteredHypoListDetail(List<HypothesediagCons> filteredHypoListDetail) {
        this.filteredHypoListDetail = filteredHypoListDetail;
    }

    /**
     * @return the filteredAnaListDetail
     */
    public List<AnalyseDemandee> getFilteredAnaListDetail() {
        return filteredAnaListDetail;
    }

    /**
     * @param filteredAnaListDetail the filteredAnaListDetail to set
     */
    public void setFilteredAnaListDetail(List<AnalyseDemandee> filteredAnaListDetail) {
        this.filteredAnaListDetail = filteredAnaListDetail;
    }

    /**
     * @return the filteredPresListDetail
     */
    public List<Prescription> getFilteredPresListDetail() {
        return filteredPresListDetail;
    }

    /**
     * @param filteredPresListDetail the filteredPresListDetail to set
     */
    public void setFilteredPresListDetail(List<Prescription> filteredPresListDetail) {
        this.filteredPresListDetail = filteredPresListDetail;
    }

    /**
     * @return the analyseDemandeeService
     */
    public AnalyseDemandeeService getAnalyseDemandeeService() {
        return analyseDemandeeService;
    }

    /**
     * @param analyseDemandeeService the analyseDemandeeService to set
     */
    public void setAnalyseDemandeeService(AnalyseDemandeeService analyseDemandeeService) {
        this.analyseDemandeeService = analyseDemandeeService;
    }

    /**
     * @return the hypothesediagConsService
     */
    public HypothesediagConsService getHypothesediagConsService() {
        return hypothesediagConsService;
    }

    /**
     * @param hypothesediagConsService the hypothesediagConsService to set
     */
    public void setHypothesediagConsService(HypothesediagConsService hypothesediagConsService) {
        this.hypothesediagConsService = hypothesediagConsService;
    }

    /**
     * @return the infoPatientBean
     */
   /* public InfoPatientBean getInfoPatientBean() {
        return infoPatientBean;
    }*/

    /**
     * @param infoPatientBean the infoPatientBean to set
     */
   /* public void setInfoPatientBean(InfoPatientBean infoPatientBean) {
        this.infoPatientBean = infoPatientBean;
    }*/

    /**
     * @return the dataListDiag
     */
    public List<HypothesediagCons> getDataListDiag() {
        return dataListDiag;
    }

    /**
     * @param dataListDiag the dataListDiag to set
     */
    public void setDataListDiag(List<HypothesediagCons> dataListDiag) {
        this.dataListDiag = dataListDiag;
    }

    /**
     * @return the selectedHypo
     */
    public HypothesediagCons getSelectedHypo() {
        return selectedHypo;
    }

    /**
     * @param selectedHypo the selectedHypo to set
     */
    public void setSelectedHypo(HypothesediagCons selectedHypo) {
        this.selectedHypo = selectedHypo;
    }

    /**
     * @return the selectedPrescription
     */
    public Prescription getSelectedPrescription() {
        return selectedPrescription;
    }

    /**
     * @param selectedPrescription the selectedPrescription to set
     */
    public void setSelectedPrescription(Prescription selectedPrescription) {
        this.selectedPrescription = selectedPrescription;
    }

    /**
     * @return the selectedAna
     */
    public AnalyseDemandee getSelectedAna() {
        return selectedAna;
    }

    /**
     * @param selectedAna the selectedAna to set
     */
    public void setSelectedAna(AnalyseDemandee selectedAna) {
        this.selectedAna = selectedAna;
    }

    /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * @return the searchPatientRecordBean
     */
    public SearchPatientRecordBean getSearchPatientRecordBean() {
        return searchPatientRecordBean;
    }

    /**
     * @param searchPatientRecordBean the searchPatientRecordBean to set
     */
    public void setSearchPatientRecordBean(SearchPatientRecordBean searchPatientRecordBean) {
        this.searchPatientRecordBean = searchPatientRecordBean;
    }

}
