/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;

/**
 *
 * @author abalo
 */
@FacesValidator(value = "lessDecimalValid")
public class LessDecimalValidator implements Validator {

   
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        BigDecimal val = null;
        InputText refComponent = null;
        BigDecimal refVal = null;
        
        try {
            refComponent = (InputText) uic.getAttributes().get("refVal");
            refVal = refComponent.getValue() != null ? (BigDecimal) refComponent.getValue() : (refComponent.getSubmittedValue() != null ? (new BigDecimal((String) refComponent.getSubmittedValue())) : null);
            //Si les valeurs saisies sont obligatoires, on laisse required faire son job; sinon si elles ne le sont pas, alors on ne valide rien si elles sont nulles
            if (o == null || String.valueOf(o).isEmpty() || refVal == null) {
                return;
            }
            val = (BigDecimal) o;

        } catch (Exception e) {
            Logger.getLogger(GreaterDecimalValidator.class.getName()).log(Level.SEVERE, null, e);
            String detail = "Erreur survenue lors de la validation de " + uic.getId() +" " + o + " avec la valeur de référence " + (refComponent != null ? refComponent.getId() : "composant inconnu") + " = " + refVal + ". Cause : " + e.getMessage();
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
        
        //Test de validation
        if (val.compareTo(refVal) == 1) {//si la valeur est > à la référence
            String detail = val + " est supérieur à " + refComponent.getId() + " = " + refVal;
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
    }

    private FacesMessage createFacesMessage(UIComponent uic, String detail) {
        FacesMessage fm = new FacesMessage();
        fm.setSummary(uic.getId() + " : ");
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        return fm;
    }

}
