/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.calendar.Calendar;

/**
 *
 * @author dateDebut
 */
@FacesValidator(value = "dateFinValid")
public class DateFinValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        Date dateFin = null;
        Calendar dateDebutComponent = null;
        String submittedDateDebut = null;
        Date dateDebut = null;
        try {
            dateDebutComponent = (Calendar) uic.getAttributes().get("dateDebut");
            dateDebut = dateDebutComponent.getValue() != null ? (Date) dateDebutComponent.getValue() : null;
            if (dateDebut == null) {
                submittedDateDebut = dateDebutComponent.getSubmittedValue() != null ? (String) dateDebutComponent.getSubmittedValue() : null;
                if (submittedDateDebut != null && !submittedDateDebut.isEmpty()) {
                    String dateDebutPattern = dateDebutComponent.getPattern();
                    dateDebut = (new SimpleDateFormat(dateDebutPattern)).parse(submittedDateDebut);
                }
            }

            //Si les dates début et fin sont obligatoires, on laisse required faire son job; sinon si elles ne le sont pas, alors on ne valide rien si elles st nulles
            if (o == null || String.valueOf(o).isEmpty() || dateDebut == null) {
                return;
            }
            dateFin = (Date) o;

            String datePart = (new SimpleDateFormat("dd/MM/yyyy")).format(dateFin);
            String dateFinStr = (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateFin);
            if (datePart == null) {
                String detail = dateFinStr + " ne respecte pas le format Date/Heure (jj/mm/aaaa hh:mi)";
                throw new ValidatorException(createFacesMessage(uic, detail));
            } else if (dateFin.getTime() < dateDebut.getTime()) {
                String detail = dateFinStr + " est antérieure à " + dateDebutComponent.getId() + " : " + (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateDebut);
                dateDebutComponent.setValid(false);
                throw new ValidatorException(createFacesMessage(uic, detail));
            }

        } catch (ValidatorException e) {
            throw e;
        } catch (Exception ex) {
            Logger.getLogger(DateFinValidator.class.getName()).log(Level.SEVERE, null, ex);
            String detail = "Erreur survenue lors de la validation de la date et heure " + o + " avec la date et heure fin " + dateDebut != null ? (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateDebut) : null + ". Cause : " + ex.getMessage();
            throw new ValidatorException(createFacesMessage(uic, detail));
        }

    }

    private FacesMessage createFacesMessage(UIComponent uic, String detail) {
        FacesMessage fm = new FacesMessage();
        fm.setSummary(uic.getId() + " : ");
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        return fm;
    }

}
