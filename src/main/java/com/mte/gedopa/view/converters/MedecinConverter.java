
package com.mte.gedopa.view.converters;

import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.service.gedopa.MedecinService;
import com.mte.gedopa.service.impl.gedopa.MedecinServiceImpl;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.Id;


/**
 *
 * @author H
 */
@FacesConverter(value="medecinConv")
public class MedecinConverter implements Converter{

    MedecinService service;
    Field idField;
    
    public MedecinConverter() {
        service = MedecinServiceImpl.getInstance();
        for(Field field : Medecin.class.getDeclaredFields()) {
            if(field.isAnnotationPresent(Id.class)) {
                idField = field;
                break;
            }
        }        
    }
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        try {
           if (string != null && string.length() > 0) {
                Map<String, Object> params = new HashMap<>();
                params.put(idField.getName(), String.valueOf(string));
                List<Medecin> list = service.findBy(params, null);
                if (list != null && !list.isEmpty()) {
                    return list.get(0);
                }
            }

            return null;
        } catch (Exception ex) {
            Logger.getLogger(MedecinConverter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(!(o instanceof Medecin)) {
            return String.valueOf(o);
        }
        try {
            idField.setAccessible(true);
            String value = String.valueOf(idField.get(o));
            idField.setAccessible(false);
            return  value;
        } catch(SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(MedecinConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}