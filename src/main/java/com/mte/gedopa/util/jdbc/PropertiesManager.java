/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.util.jdbc;

import com.mte.gedopa.util.*;
import java.util.ResourceBundle;

/**
 *
 * @author H
 */
public class PropertiesManager {

    private static ResourceBundle dbProperties = null;
    
    public static ResourceBundle getDbProperties() {
        dbProperties = ResourceBundle.getBundle("com.mte.gedopa.Db");
        return dbProperties;
    }
}
