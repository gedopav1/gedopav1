package com.mte.gedopa.util.jdbc;

import com.mte.gedopa.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author H
 */
public class Connector {

    private static Connection adminConnection;
    private static final ResourceBundle dbProps = PropertiesManager.getDbProperties();;
    private static Connection dataConnection;

    public static Connection getAdminConnection() {
        String user = dbProps.getString("admin.user");
        String pass = dbProps.getString("admin.pass");
        return getAdminConnection(user, pass);

    }

    public static Connection getAdminConnection(String user, String password) {
        //Mise en oeuvre d'un singleton de connection
        try {
            if (adminConnection == null || adminConnection.isClosed()) {
                openAdminConnection(user, password);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return adminConnection;
    }

    private static void openAdminConnection(String user, String password) {
        String driver = dbProps.getString("db.driver");
        String url = dbProps.getString("admin.db.url");
        try {
            Class.forName(driver);
            adminConnection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void closeConnections() {
    	try {
    	if (adminConnection != null && !adminConnection.isClosed()) {
            adminConnection.close();
        }
    	if (dataConnection != null && !dataConnection.isClosed()) {
    		dataConnection.close();
        }
    	} catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
    public static Connection getDataConnection() {
        
        String user = dbProps.getString("data.user");
        String pass = dbProps.getString("data.pass");
        return getDataConnection(user, pass);

    }

    public static Connection getDataConnection(String user, String password) {
        //Mise en oeuvre d'un singleton de connection
        try {
            if (dataConnection == null || dataConnection.isClosed()) {
            	connectToData(user, password);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return dataConnection;
    }

    private static void connectToData(String user, String password) {
        String driver = dbProps.getString("db.driver");
        String url = dbProps.getString("data.db.url");
        try {
            Class.forName(driver);
            dataConnection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void p(String s) {
        System.out.println(s);
    }

    public static void main(String args[]) {
    	Connection conn = getDataConnection();
        System.out.println("conn = "+conn);


//    	System.out.println("Date = " + HoraDate.valueOf("16/10/2011").getTime());
//    	System.out.println((new Date()).getTime());
    }
}
