/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.util.servlets;

import com.mte.gedopa.service.admin.SysParamService;
import com.mte.gedopa.service.impl.admin.SysParamServiceImpl;
import com.mte.gedopa.util.IConstants;
import com.mte.gedopa.util.jdbc.Connector;
//import com.mte.gedopa.util.jdbc.Connector;
//import com.mte.gedopa.util.Connector;
import java.io.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

/**
 *
 * @author abalo
 */
public class JasperServlet extends HttpServlet {

    private String whiteLogo = "/img/logos/logo_sipe.png";
    private SysParamService sps;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        OutputStream out = response.getOutputStream();

        try {
            sps = SysParamServiceImpl.getInstance();
            String conn = request.getParameter("conn");
            String rep = request.getParameter("rep");
            String format = (request.getParameter("format") != null) ? request.getParameter("format") : "html";

            Map<String, Object> reportParams = new HashMap<String, Object>();
            reportParams.put("LOGO_URL", sps.getValue("LOGO_URL"));

            String saveToFile = request.getParameter("save_to_file");
            String saveAs = request.getParameter("save_as");

            for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements();) {
                String parameterName = e.nextElement();
                String parameterValue = request.getParameter(parameterName);
                if (parameterName.equalsIgnoreCase("ID_DEBUT") || parameterName.equalsIgnoreCase("ID_FIN")) {
                    reportParams.put(parameterName.toUpperCase(), new Integer(parameterValue));
                } else if (parameterName.equalsIgnoreCase("DATE_HEURE_DEBUT") || parameterName.equalsIgnoreCase("DATE_HEURE_FIN")) {
                    reportParams.put(parameterName.toUpperCase(), new Timestamp(Long.valueOf(parameterValue)));
                } else if (parameterName.equalsIgnoreCase("DATE_DEBUT") || parameterName.equalsIgnoreCase("DATE_FIN")) {
                    long dateMillis = Long.valueOf(parameterValue);
                    reportParams.put(parameterName.toUpperCase(), new Date(dateMillis));
                } else if (parameterName.equalsIgnoreCase("PAPIER")) {
                    reportParams.put(parameterName.toUpperCase(), (parameterValue == null ? "blanc" : parameterValue));
                } else if (parameterName.equalsIgnoreCase("SCREEN_ID")
                        || parameterName.equalsIgnoreCase("MENU_ITEM_ID")
                        || parameterName.equalsIgnoreCase("MENU_ID")
                        || parameterName.equalsIgnoreCase("LOG_ID")) {
                    reportParams.put(parameterName.toUpperCase(), new Integer(parameterValue));
                } else {
                    String val = request.getParameter(parameterName);
                    reportParams.put(parameterName.toUpperCase(), val);
                }
            }

            
            reportParams.put(IConstants.SUBREPORT_DIR, sps.getValue(IConstants.SUBREPORT_DIR));
            System.out.println("sps.getValue(IConstants.SUBREPORT_DIR) = " + sps.getValue(IConstants.SUBREPORT_DIR));
            //InputStream designStream = new FileInputStream(getServletContext().getRealPath("/reports/" + doc));
            InputStream designStream = new FileInputStream(getServletContext().getRealPath("/reports/" + rep));
            System.out.println("designStream = " + (designStream != null ? designStream + " : " + designStream.available() : designStream));
            if (reportParams.containsKey("LOGO_URL")) {
                String path = (String) reportParams.get("LOGO_URL");
                String url = (getServletContext().getResource(path) != null) ? getServletContext().getResource(path).toString() : getServletContext().getResource(whiteLogo).toString();
                reportParams.remove("LOGO_URL");
                reportParams.put("LOGO_URL", url);
            }

            JasperPrint jasperPrint = returnReportPrint(reportParams, designStream);
            JRExporter exporter = null; 
            out.write(designStream.read());
            if (jasperPrint == null) {
                out.write("No report generated!".getBytes());
               
            } else {
                if (format.equalsIgnoreCase(IConstants.PDF_FORMAT)) {
                    exporter = new JRPdfExporter();
                    response.setContentType("application/pdf");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
                            out);
                    if (saveToFile != null && saveToFile.equalsIgnoreCase("Y") && saveAs != null) {
                        String fullQualName = sps.getValue(IConstants.EXPORT_DOCS_PATH) + saveAs + ".pdf";
                        System.out.println("fullQualName = " + fullQualName);
                        JasperExportManager.exportReportToPdfFile(jasperPrint, fullQualName);
                    }

                } else if (format.equalsIgnoreCase(IConstants.XLS_FORMAT)) {
                    exporter = new JRXlsExporter();
                    response.setContentType("application/xls");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
                            out);

                } else if (format.equalsIgnoreCase(IConstants.XLSX_FORMAT)) {
                    exporter = new JRXlsxExporter();
                    response.setContentType("application/xlsx");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
                            out);

                } else if (format.equalsIgnoreCase(IConstants.CSV_FORMAT)) {
                    exporter = new JRCsvExporter();
                    response.setContentType("text/plain");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
                            out);

                } else if (format.equalsIgnoreCase(IConstants.WORD_FORMAT)) {
                    exporter = new JROdtExporter();
                    response.setContentType("application/msword");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,
                            jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
                            out);

                } else {
                    exporter = new JRHtmlExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                    if (saveToFile != null && saveToFile.equalsIgnoreCase("Y") && saveAs != null) {
                        String fullQualName = sps.getValue(IConstants.EXPORT_DOCS_PATH) + saveAs + ".html";
                        System.out.println("fullQualName = " + fullQualName);
                        JasperExportManager.exportReportToHtmlFile(jasperPrint, fullQualName);
                    }
                }

                // Make images available for the HTML output
                request.getSession().setAttribute(
                        ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE,
                        jasperPrint);
                exporter.setParameter(
                        JRHtmlExporterParameter.IMAGES_MAP, new HashMap());
                // The following statement requires mapping /image
                // to the imageServlet in the web.xml.
                //
                // This servlet serves up images including the px
                // images for spacing.
                //

                exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,
                        request.getContextPath() + "/image?image=");

                exporter.exportReport();
            }

        } catch (Exception e) {
            System.out.println("Erreur " + e.getMessage());
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }

    }

    /**
     * Prend 2 paramètres: parameters map, jrxml template stream et prépare
     * l'état de sortie.
     *
     * @param params le map des paramètres,
     * @param inputStream le flux de fichier en lecture du template jrxml de
     * l'état
     * @return jasperReport le jasperReport qui sera utilisé pour exporter
     * l'état dans un format donné
     */
    public JasperPrint returnReportPrint(Map<String, Object> params, InputStream inputStream) {
        
        JasperPrint jasperPrint = null;
        try {
            //JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            Connection jdbcConnection = Connector.getDataConnection();
            System.out.println("jdbcConnection = " + jdbcConnection);
            //jasperPrint = JasperFillManager.fillReport(jasperReport, params, jdbcConnection);
            jasperPrint = JasperFillManager.fillReport(inputStream, params, jdbcConnection);
        } catch (Exception e) {
            Logger.getLogger(JasperServlet.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            try {
                inputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(JasperServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("jasperPrint = " + jasperPrint);
        return jasperPrint;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
